# GameLibrary

A GTK/TS prototype of a video game library manager and launcher for Linux.

## Build dependencies that I remember (gentoo)
- `dev-lang/typescript`
- `dev-libs/gobject-introspection`
- `dev-util/blueprint-compiler`
- `dev-libs/glib`
- `>=gui-libs/gtk-4.12*`
- `>=gui-libs/libadwaita-1.4.0`