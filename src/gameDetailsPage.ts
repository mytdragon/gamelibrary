import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import Game from "./database/models/game.js";

import { formatDate } from "./utils/date.js";

export default class GameDetailsPage extends Adw.NavigationPage {
    private _boxAgeRatings!: Gtk.Box;
    private _boxCommunityRating!: Gtk.Box;
    private _boxCompletionStatus!: Gtk.Box;
    private _boxCriticRating!: Gtk.Box;
    private _boxDescription!: Gtk.Box;
    private _boxDevelopers!: Gtk.Box;
    private _boxGameModes!: Gtk.Box;
    private _boxGenres!: Gtk.Box;
    private _boxKeywords!: Gtk.Box;
    private _boxLinks!: Gtk.Box;
    private _boxNotes!: Gtk.Box;
    private _boxPlatforms!: Gtk.Box;
    private _boxPlayerPerspectives!: Gtk.Box;
    private _boxPublishers!: Gtk.Box;
    private _boxRegions!: Gtk.Box;
    private _boxReleaseDate!: Gtk.Box;
    private _boxSeries!: Gtk.Box;
    private _boxSource!: Gtk.Box;
    private _boxThemes!: Gtk.Box;
    private _boxUserRating!: Gtk.Box;
    private _clampCoverNone!: Adw.Clamp;
    private _clampCoverPicture!: Adw.Clamp;
    private _labelAgeRatings!: Gtk.Label;
    private _labelCommunityRating!: Gtk.Label;
    private _labelCompletionStatus!: Gtk.Label;
    private _labelCriticRating!: Gtk.Label;
    private _labelDescription!: Gtk.Label;
    private _labelDevelopers!: Gtk.Label;
    private _labelGameModes!: Gtk.Label;
    private _labelGenres!: Gtk.Label;
    private _labelKeywords!: Gtk.Label;
    private _labelLastPlayed!: Gtk.Label;
    private _labelName!: Gtk.Label;
    private _labelNotes!: Gtk.Label;
    private _labelPlatforms!: Gtk.Label;
    private _labelPlayTime!: Gtk.Label;
    private _labelPlayerPerspectives!: Gtk.Label;
    private _labelPublishers!: Gtk.Label;
    private _labelRegions!: Gtk.Label;
    private _labelReleaseDate!: Gtk.Label;
    private _labelSeries!: Gtk.Label;
    private _labelSource!: Gtk.Label;
    private _labelThemes!: Gtk.Label;
    private _labelUserRating!: Gtk.Label;
    private _listBoxLinks!: Gtk.ListBox;
    private _overlayBackground!: Gtk.Overlay;
    private _pictureBlurredBackground!: Gtk.Picture;
    private _pictureCover!: Gtk.Picture;

    private game: Game;
    private database = Database.getInstance();

    static {
        GObject.registerClass({
            GTypeName: "GameDetailsPage",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/game-details-page.ui",
            InternalChildren: [
                "boxAgeRatings",
                "boxCompletionStatus",
                "boxCommunityRating",
                "boxCriticRating",
                "boxDescription",
                "boxDevelopers",
                "boxGameModes",
                "boxGenres",
                "boxKeywords",
                "boxLinks",
                "boxNotes",
                "boxPlatforms",
                "boxPlayerPerspectives",
                "boxPublishers",
                "boxRegions",
                "boxReleaseDate",
                "boxSeries",
                "boxSource",
                "boxThemes",
                "boxUserRating",
                "clampCoverNone",
                "clampCoverPicture",
                "labelAgeRatings",
                "labelCompletionStatus",
                "labelCommunityRating",
                "labelCriticRating",
                "labelDescription",
                "labelDevelopers",
                "labelGameModes",
                "labelGenres",
                "labelKeywords",
                "labelLastPlayed",
                "labelName",
                "labelNotes",
                "labelPlatforms",
                "labelPlayTime",
                "labelPlayerPerspectives",
                "labelPublishers",
                "labelRegions",
                "labelReleaseDate",
                "labelSeries",
                "labelSource",
                "labelThemes",
                "labelUserRating",
                "listBoxLinks",
                "overlayBackground",
                "pictureBlurredBackground",
                "pictureCover"
            ]
        }, this);
    }

    constructor(game: Game) {
        super();

        this.game = game;

        this.setupUi();

        this.database.gamesCollection.connect("item-updated", (_, game) => {
            if (game.id !== this.game.id) return;
            this.game = game;
            this.setupUi();
        });
    }

    private setupUi() {
        this.set_title(this.game.name);
        this._labelName.set_text(this.game.name);

        if (this.game.backgroundImage) {
            this._pictureBlurredBackground.set_file(this.database.getFile(this.game.backgroundImage));
            this._overlayBackground.set_visible(true);
            this.set_css_classes(["background-image"]);
        }
        else {
            this.set_css_classes([]);
            this._overlayBackground.set_visible(false);
        }

        if (this.game.coverImage) {
            this._pictureCover.set_file(this.database.getFile(this.game.coverImage));
            this._clampCoverNone.set_visible(false);
            this._clampCoverPicture.set_visible(true);
        }
        else {
            this._clampCoverNone.set_visible(true);
            this._clampCoverPicture.set_visible(false);
        }

        this._labelPlayTime.set_label(this.game.getFormattedPlayTime());
        this._labelLastPlayed.set_label(this.game.lastPlayed ? formatDate(this.game.lastPlayed, "%d/%m/%Y") : _("Not played"));

        if (this.game.sourceId) {
            this._labelSource.set_label(this.database.sourcesCollection.getById(this.game.sourceId)?.name ?? _("(Unknown)"));
            this._boxSource.set_visible(true);
        }
        else this._boxSource.set_visible(false);

        if (this.game.completionStatusId) {
            this._labelCompletionStatus.set_label(this.database.completionStatusesCollection.getById(this.game.completionStatusId)?.name ?? _("(Unknown)"));
            this._boxCompletionStatus.set_visible(true);
        }
        else this._boxCompletionStatus.set_visible(false);

        if (this.game.releaseDate) {
            this._labelReleaseDate.set_label(formatDate(this.game.releaseDate, "%d/%m/%Y"));
            this._boxReleaseDate.set_visible(true);
        }
        else this._boxReleaseDate.set_visible(false);

        if (this.game.platformIds.length > 0) {
            this._labelPlatforms.set_label(this.game.platformIds.map(id => this.database.platformsCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxPlatforms.set_visible(true);
        }
        else this._boxPlatforms.set_visible(false);

        if (this.game.developerIds.length > 0) {
            this._labelDevelopers.set_label(this.game.developerIds.map(id => this.database.developersCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxDevelopers.set_visible(true);
        }
        else this._boxDevelopers.set_visible(false);

        if (this.game.publisherIds.length > 0) {
            this._labelPublishers.set_label(this.game.publisherIds.map(id => this.database.publishersCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxPublishers.set_visible(true);
        }
        else this._boxPublishers.set_visible(false);

        if (this.game.gameModeIds.length > 0) {
            this._labelGameModes.set_label(this.game.gameModeIds.map(id => this.database.gameModesCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxGameModes.set_visible(true);
        }
        else this._boxGameModes.set_visible(false);

        if (this.game.genreIds.length > 0) {
            this._labelGenres.set_label(this.game.genreIds.map(id => this.database.genresCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._labelGenres.set_visible(true);
        }
        else this._boxGenres.set_visible(false);

        if (this.game.playerPerspectiveIds.length > 0) {
            this._labelPlayerPerspectives.set_label(this.game.playerPerspectiveIds.map(id => this.database.playerPerspectivesCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxPlayerPerspectives.set_visible(true);
        }
        else this._boxPlayerPerspectives.set_visible(false);

        if (this.game.themeIds.length > 0) {
            this._labelThemes.set_label(this.game.themeIds.map(id => this.database.themesCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxThemes.set_visible(true);
        }
        else this._boxThemes.set_visible(false);

        if (this.game.keywordIds.length > 0) {
            this._labelKeywords.set_label(this.game.keywordIds.map(id => this.database.keywordsCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxKeywords.set_visible(true);
        }
        else this._boxKeywords.set_visible(false);

        if (this.game.serieIds.length > 0) {
            this._labelSeries.set_label(this.game.serieIds.map(id => this.database.seriesCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxSeries.set_visible(true);
        }
        else this._boxSeries.set_visible(false);

        if (this.game.ageRatingIds.length > 0) {
            this._labelAgeRatings.set_label(this.game.ageRatingIds.map(id => this.database.ageRatingsCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxAgeRatings.set_visible(true);
        }
        else this._boxAgeRatings.set_visible(false);

        if (this.game.regionIds.length > 0) {
            this._labelRegions.set_label(this.game.regionIds.map(id => this.database.regionsCollection.getById(id)?.name).filter(item => item !== undefined).join("\n"));
            this._boxRegions.set_visible(true);
        }
        else this._boxRegions.set_visible(false);

        if (this.game.userRating) {
            this._labelUserRating.set_label(this.game.userRating.toString());
            this._boxUserRating.set_visible(true);
        }
        else this._boxUserRating.set_visible(false);

        if (this.game.communityRating) {
            this._labelCommunityRating.set_label(this.game.communityRating.toString());
            this._boxCommunityRating.set_visible(true);
        }
        else this._boxCommunityRating.set_visible(false);

        if (this.game.criticRating) {
            this._labelCriticRating.set_label(this.game.criticRating.toString());
            this._boxCriticRating.set_visible(true);
        }
        else this._boxCriticRating.set_visible(false);

        if (this.game.links.length > 0) {
            this._boxLinks.set_visible(true);
            this._listBoxLinks.remove_all();

            for (const link of this.game.links) {
                this._listBoxLinks.append(new Gtk.ListBoxRow({
                    activatable: false,
                    child: new Gtk.LinkButton({ label: link.title, uri: link.url, cssClasses: [], halign: Gtk.Align.START })
                }));
            }
        }
        else this._boxLinks.set_visible(false);

        this._boxDescription.set_visible(this.game.description !== null);
        this._labelDescription.set_label(this.game.description ?? "");

        this._boxNotes.set_visible(this.game.notes !== null);
        this._labelNotes.set_label(this.game.notes ?? "");
    }
}