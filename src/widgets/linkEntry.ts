import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

interface ConstructorProperties extends Gtk.ListBoxRow.ConstructorProperties {
    title: string;
    url: string;
};

export default class LinkEntry extends Gtk.ListBoxRow {
    public moveDownButton: Gtk.Button;
    public moveUpButton: Gtk.Button;
    public removeButton: Gtk.Button;

    public title!: string;
    public url!: string;

    static {
        GObject.registerClass({
            GTypeName: "LinkEntry",
            Properties: {
                "title": GObject.ParamSpec.string("title", "Title", "The title of the link", GObject.ParamFlags.READWRITE, ""),
                "url": GObject.ParamSpec.string("url", "URL", "The url of the link", GObject.ParamFlags.READWRITE, "")
            }
        }, this);
    }

    constructor(props?: Partial<ConstructorProperties>) {
        super(props);

        this.set_activatable(false);
        this.set_css_classes(["link-entry"]);
        
        const hbox = new Gtk.Box({
            orientation: Gtk.Orientation.HORIZONTAL,
            spacing: 10
        });
        const titleEntry = new Gtk.Entry({ width_request: 100, placeholderText: _("Name"), text: this.title});
        titleEntry.bind_property("text", this, "title", GObject.BindingFlags.BIDIRECTIONAL);
        const urlEntry = new Gtk.Entry({ hexpand: true, placeholderText: _("Url"), text: this.url });
        urlEntry.bind_property("text", this, "url", GObject.BindingFlags.BIDIRECTIONAL);
        this.moveUpButton = new Gtk.Button({ iconName: "go-up-symbolic", tooltipText: _("Move up") });
        this.moveDownButton = new Gtk.Button({ iconName: "go-down-symbolic", tooltipText: _("Move down") });
        this.removeButton = new Gtk.Button({ iconName: "edit-delete-symbolic", tooltipText: _("Remove") });

        hbox.append(titleEntry);
        hbox.append(urlEntry);
        hbox.append(this.moveUpButton);
        hbox.append(this.moveDownButton);
        hbox.append(this.removeButton);
        
        this.set_child(hbox);
    }
}