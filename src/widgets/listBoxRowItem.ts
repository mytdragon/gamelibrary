import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

const PARAM_FLAGS = GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY | GObject.ParamFlags.STATIC_NICK | GObject.ParamFlags.STATIC_BLURB;

export default class ListBoxRowItem extends Gtk.ListBoxRow {
    public item!: GObject.Object | Gtk.StringObject;

    static {
        GObject.registerClass({
            GTypeName: "ListBoxRowItem",
            Properties: {
                Item: GObject.ParamSpec.object("item", "Item", "The item binded to the row.", PARAM_FLAGS, GObject.TYPE_OBJECT)
            }
        }, this);
    }
}