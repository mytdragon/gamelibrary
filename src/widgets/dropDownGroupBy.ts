import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import {
    Model,
    CompletionStatus,
    Game
} from "../database/models/index.js";

import ListBoxRowItem from "./listBoxRowItem.js";

interface GroupField extends GObject.Object {
    label: string;
    field: keyof Game | null;
    model?: typeof Model;
};

export default class DropDownGrouptBy extends Gtk.Box {
    private _listBox!: Gtk.ListBox;
    private _popover!: Gtk.Popover;
    private _toggleButtonOpen!: Gtk.ToggleButton;

    public readonly GROUP_FIELDS = [
        { label: _("Don't group"), field: null },
        { label: _("Completion status"), field: "completionStatusId", model: CompletionStatus }
    ];

    private groupField: GroupField | null = null;

    static {
        GObject.registerClass({
            CssName: "dropdown",
            GTypeName: "DropDownGroupBy",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/widgets/drop-down-group-by.ui",
            InternalChildren: [
                "listBox",
                "popover",
                "toggleButtonOpen"
            ],
            Signals: {
                "changed": {
                    param_types: [GObject.TYPE_OBJECT]
                }
            }
        }, this);
    }

    constructor() {
        super();

        const listStoreGroupFields = new Gio.ListStore(GObject.TYPE_OBJECT);
        const groupFields = this.GROUP_FIELDS.map(groupField => Object.assign(new GObject.Object, groupField));
        listStoreGroupFields.splice(0, 0, groupFields);
        const sortModelGroupFields = new Gtk.SortListModel({
            model: listStoreGroupFields,
            sorter: Gtk.CustomSorter.new((a: GroupField, b: GroupField) => {
                if (a.field === null) return -1;
                else if (b.field === null) return 1;
                else return a.label.localeCompare(b.label)
            })
        });

        let defaultRowGroupField: ListBoxRowItem | null = null;
        this._listBox.bind_model(sortModelGroupFields, (object) => {
            const groupField = object as GroupField;
            
            const listBoxRowItem = new ListBoxRowItem();
            listBoxRowItem.item = object;
            const box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL, spacing: 5 });
            const label = new Gtk.Label({ xalign: 0, label: groupField.label });
            const image = new Gtk.Image({ icon_name: "object-select-symbolic", opacity: 0 });

            if (groupField.field === null) defaultRowGroupField = listBoxRowItem;

            box.append(image);
            box.append(label);
            listBoxRowItem.set_child(box);

            return listBoxRowItem;
        });

        this._listBox.connect("row-selected", (_source, listBoxRow) => this.onRowSelectedGroupField(listBoxRow));

        this._listBox.connect("selected-rows-changed", () => {
            let i = 0;
            let listBoxRow = this._listBox.get_row_at_index(i);
            while (listBoxRow !== null) {
                const image = (listBoxRow.get_child() as Gtk.Box).get_first_child() as Gtk.Image;
                if (listBoxRow.is_selected()) image.set_opacity(100);
                else image.set_opacity(0);

                i++;
                listBoxRow = this._listBox.get_row_at_index(i);
            }
        });

        if (defaultRowGroupField) {
            this._listBox.select_row(defaultRowGroupField);
            this.groupField = (defaultRowGroupField as ListBoxRowItem).item as GroupField;
        }
    }

    private onPopoverClosed() {
        this._toggleButtonOpen.set_active(false);
    }

    private onToggleButtoOpenToggled() {
        if (this._toggleButtonOpen.get_active()) this._popover.popup();
        else this._popover.popdown();

        // Update popover width to be the same as button
        this._popover.set_property("width-request", this._toggleButtonOpen.get_allocated_width());

        // @ts-expect-error For some reason it doesn't accept empty constructor but there is no error
        const activeValue = new GObject.Value();
        activeValue.init(GObject.TYPE_INT);
        activeValue.set_int(this._toggleButtonOpen.get_active() ? 1 : 0);
        this.update_state([Gtk.AccessibleState.EXPANDED], [activeValue]);
    }

    private onRowSelectedGroupField(listBoxRow: Gtk.ListBoxRow | null) {
        if (listBoxRow === null || !(listBoxRow instanceof ListBoxRowItem)) return;

        const item = listBoxRow.item as GroupField;

        if (item !== this.groupField) {
            this.groupField = item;
            this.emit("changed", this.groupField);
            this._toggleButtonOpen.set_active(false);
        }
    }
}