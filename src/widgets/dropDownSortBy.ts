import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import {
    Model,

    CompletionStatus,
    Game,
    Source
} from "../database/models/index.js";
import Database from "../database/database.js";

import ListBoxRowItem from "./listBoxRowItem.js";

interface SortableField extends GObject.Object {
    label: string;
    field: keyof Game;
    model?: typeof Model;
};

enum SortDirection { ASCENDING, DESCENDING }

interface SortDirectionItem extends GObject.Object {
    label: string;
    value: SortDirection;
};

export default class DropDownSortBy extends Gtk.Box {
    private _listBoxSortableFields!: Gtk.ListBox;
    private _listBoxSortDirections!: Gtk.ListBox;
    private _popover!: Gtk.Popover;
    private _toggleButtonOpen!: Gtk.ToggleButton;

    private collections = Database.getInstance().collectionsMap;

    public readonly SORT_DIRECTIONS = [
        { label: _("Ascending"), value: SortDirection.ASCENDING },
        { label: _("Descending"), value: SortDirection.DESCENDING }
    ];

    public readonly SORTABLE_FIELDS = [
        { label: _("Community rating"), field: "communityRating" },
        { label: _("Completion status"), field: "completionStatusId", model: CompletionStatus },
        { label: _("Creation date"), field: "created" },
        { label: _("Critic rating"), field: "criticRating" },
        { label: _("Last played"), field: "lastPlayed" },
        { label: _("Modification date"), field: "modified" },
        { label: _("Play time"), field: "playTime" },
        { label: _("Name"), field: "name" },
        { label: _("Release date"), field: "releaseDate" },
        { label: _("Source"), field: "sourceId", model: Source },
        { label: _("User rating"), field: "userRating" },
    ];

    private sortableField: SortableField | null = null;
    private sortDirection: SortDirection = SortDirection.ASCENDING;

    static {
        GObject.registerClass({
            CssName: "dropdown",
            GTypeName: "DropDownSortBy",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/widgets/drop-down-sort-by.ui",
            InternalChildren: [
                "listBoxSortableFields",
                "listBoxSortDirections",
                "popover",
                "toggleButtonOpen"
            ],
            Signals: {
                "order-changed": {
                    param_types: [GObject.TYPE_OBJECT]
                },
                "sort-field-changed": {
                    param_types: [GObject.TYPE_OBJECT]
                }
            }
        }, this);
    }

    constructor() {
        super();

        // Sort direction
        const listStoreSortDirections = new Gio.ListStore(GObject.TYPE_OBJECT);
        const sortDirectionItems = this.SORT_DIRECTIONS.map(sortDirection => Object.assign(new GObject.Object, sortDirection));
        listStoreSortDirections.splice(0, 0, sortDirectionItems);
        const sortModelSortDirections = new Gtk.SortListModel({
            model: listStoreSortDirections,
            sorter: Gtk.CustomSorter.new((a: SortDirectionItem, b: SortDirectionItem) => a.label.localeCompare(b.label))
        });

        let defaultRowSortDirection;
        this._listBoxSortDirections.bind_model(sortModelSortDirections, (object) => {
            const sortDirection = object as SortDirectionItem;

            const listBoxRowItem = new ListBoxRowItem();
            listBoxRowItem.item = object;
            const box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL, spacing: 5 });
            const label = new Gtk.Label({ xalign: 0, label: sortDirection.label });
            const image = new Gtk.Image({ icon_name: "object-select-symbolic", opacity: 0 });

            if (sortDirection.value === SortDirection.ASCENDING) defaultRowSortDirection = listBoxRowItem;

            box.append(image);
            box.append(label);
            listBoxRowItem.set_child(box);

            return listBoxRowItem;
        });

        this._listBoxSortDirections.connect("row-selected", (_source, listBoxRow) => this.onRowSelectedOrder(listBoxRow));

        this._listBoxSortDirections.connect("selected-rows-changed", () => {
            let i = 0;
            let listBoxRow = this._listBoxSortDirections.get_row_at_index(i);
            while (listBoxRow !== null) {
                const image = (listBoxRow.get_child() as Gtk.Box).get_first_child() as Gtk.Image;
                if (listBoxRow.is_selected()) image.set_opacity(100);
                else image.set_opacity(0);

                i++;
                listBoxRow = this._listBoxSortDirections.get_row_at_index(i);
            }
        });

        if (defaultRowSortDirection) {
            this._listBoxSortDirections.select_row(defaultRowSortDirection);
            this.sortDirection = ((defaultRowSortDirection as ListBoxRowItem).item as SortDirectionItem).value;
        }

        // Fields
        const listStoreSortableFields = new Gio.ListStore(GObject.TYPE_OBJECT);
        const sortableFieldItems = this.SORTABLE_FIELDS.map(sortableField => Object.assign(new GObject.Object, sortableField));
        listStoreSortableFields.splice(0, 0, sortableFieldItems);
        const sortModelSortableFields = new Gtk.SortListModel({
            model: listStoreSortableFields,
            sorter: Gtk.CustomSorter.new((a: SortableField, b: SortableField) => a.label.localeCompare(b.label))
        });

        let defaultRowSortField: ListBoxRowItem | null = null;
        this._listBoxSortableFields.bind_model(sortModelSortableFields, (object) => {
            const sortableField = object as SortableField;
            
            const listBoxRowItem = new ListBoxRowItem();
            listBoxRowItem.item = object;
            const box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL, spacing: 5 });
            const label = new Gtk.Label({ xalign: 0, label: sortableField.label });
            const image = new Gtk.Image({ icon_name: "object-select-symbolic", opacity: 0 });

            if (sortableField.field === "name") defaultRowSortField = listBoxRowItem;

            box.append(image);
            box.append(label);
            listBoxRowItem.set_child(box);

            return listBoxRowItem;
        });

        this._listBoxSortableFields.connect("row-selected", (_source, listBoxRow) => this.onRowSelectedSortField(listBoxRow));

        this._listBoxSortableFields.connect("selected-rows-changed", () => {
            let i = 0;
            let listBoxRow = this._listBoxSortableFields.get_row_at_index(i);
            while (listBoxRow !== null) {
                const image = (listBoxRow.get_child() as Gtk.Box).get_first_child() as Gtk.Image;
                if (listBoxRow.is_selected()) image.set_opacity(100);
                else image.set_opacity(0);

                i++;
                listBoxRow = this._listBoxSortableFields.get_row_at_index(i);
            }
        });

        if (defaultRowSortField) {
            this._listBoxSortableFields.select_row(defaultRowSortField);
            this.sortableField = (defaultRowSortField as ListBoxRowItem).item as SortableField;
        }
    }

    private onPopoverClosed() {
        this._toggleButtonOpen.set_active(false);
    }

    private onToggleButtoOpenToggled() {
        if (this._toggleButtonOpen.get_active()) this._popover.popup();
        else this._popover.popdown();

        // Update popover width to be the same as button
        this._popover.set_property("width-request", this._toggleButtonOpen.get_allocated_width());

        // @ts-expect-error For some reason it doesn't accept empty constructor but there is no error
        const activeValue = new GObject.Value();
        activeValue.init(GObject.TYPE_INT);
        activeValue.set_int(this._toggleButtonOpen.get_active() ? 1 : 0);
        this.update_state([Gtk.AccessibleState.EXPANDED], [activeValue]);
    }

    private onRowSelectedOrder(listBoxRow: Gtk.ListBoxRow | null) {
        if (listBoxRow === null || !(listBoxRow instanceof ListBoxRowItem)) return;

        const item = listBoxRow.item as SortDirectionItem;

        if (item.value !== this.sortDirection) {
            this.sortDirection = item.value;
            this.emit("order-changed", item);
        }
    }

    private onRowSelectedSortField(listBoxRow: Gtk.ListBoxRow | null) {
        if (listBoxRow === null || !(listBoxRow instanceof ListBoxRowItem)) return;

        const item = listBoxRow.item as SortableField;

        if (item !== this.sortableField) {
            this.sortableField = item;
            this.emit("sort-field-changed", this.sortableField);
        }
    }

    public compare(gameA: Game, gameB: Game) {
        switch (this.sortableField?.field) {
            case "completionStatusId":
            case "sourceId":
                if (!this.sortableField.model) throw new Error(`Sortable field "${this.sortableField.field}" has missing model.`);

                const collection = this.collections.get(this.sortableField.model);
                if (!collection) throw new Error(`Couldn't map collection with "${this.sortableField.model}".`);

                const idA = gameA[this.sortableField.field];
                const idB = gameB[this.sortableField.field];

                if (idA === null && idB === null) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (idA === null) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else if (idB === null) return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;

                const a = collection.getById(idA);
                const b = collection.getById(idB);

                if (a === null && b === null) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (a === null) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else if (b === null) return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;

                return this.sortDirection === SortDirection.ASCENDING ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name);

            case "created":
            case "lastPlayed":
            case "modified":
            case "releaseDate":
                const dateA = gameA[this.sortableField.field];
                const dateB = gameB[this.sortableField.field];

                if (dateA === null && dateB === null) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (dateA === null) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else if (dateB === null) return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;

                const timeA = dateA.getTime();
                const timeB = dateB.getTime();

                if (timeA === timeB) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (timeA < timeB) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;
            
            case "criticRating":      
            case "communityRating":
            case "playTime":
            case "userRating":
                const valueA = gameA[this.sortableField.field];
                const valueB = gameB[this.sortableField.field];

                if (valueA === null && valueB === null) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (valueA === null) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else if (valueB === null) return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;

                if (valueA === valueB) return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
                else if (valueA < valueB) return this.sortDirection === SortDirection.ASCENDING ? -1 : 1;
                else return this.sortDirection === SortDirection.ASCENDING ? 1 : -1;

            default:
                return this.sortDirection === SortDirection.ASCENDING ? gameA.name.localeCompare(gameB.name) : gameB.name.localeCompare(gameA.name);
        }
    }
}