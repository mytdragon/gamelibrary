import Gdk from "gi://Gdk?version=4.0";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import ItemCollection from "../database/collections/itemCollection.js";
import Model from "../database/models/model.js";

import ListBoxRowItem from "./listBoxRowItem.js";

type ConstructorProperties = {
    enableFilter?: boolean;
    enableMulti?: boolean;
    expression?: Gtk.Expression;
    items?: GObject.Object[] | Gtk.StringObject[];
    searchMatchMode?: Gtk.StringFilterMatchMode;
};

const PARAM_FLAGS = GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY | GObject.ParamFlags.STATIC_NICK | GObject.ParamFlags.STATIC_BLURB;

export default class DropDown extends Gtk.Box {
    private _collection!: ItemCollection<Model> | null;
    private _enableFilter!: boolean;
    private _enableMulti!: boolean;
    private _enableSort!: boolean;
    private _expression!: Gtk.Expression | null;
    private _labelSelected!: Gtk.Label;
    private _labelSelectionEmpty!: Gtk.Label;
    private _listBox!: Gtk.ListBox;
    private _popover!: Gtk.Popover;
    private _searchEntry!: Gtk.SearchEntry;
    private _toggleButtonOpen!: Gtk.ToggleButton;
    private _toggleButtonSelected!: Gtk.ToggleButton;
    private _items!: GObject.Object[] | Gtk.StringObject[];
    private _searchMatchMode: Gtk.StringFilterMatchMode = Gtk.StringFilterMatchMode.PREFIX;

    private selection: Map<GObject.Object, ListBoxRowItem> = new Map();

    static {
        GObject.registerClass({
            CssName: "dropdown",
            GTypeName: "DropDown",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/widgets/drop-down.ui",
            InternalChildren: [
                "labelSelected",
                "labelSelectionEmpty",
                "listBox",
                "popover",
                "searchEntry",
                "toggleButtonOpen",
                "toggleButtonSelected"
            ],
            Properties: {
                Collection: GObject.ParamSpec.object("collection", "Collection", "The collection of the drop down.", PARAM_FLAGS, ItemCollection<Model>),
                EnableFilter: GObject.ParamSpec.boolean("enable-filter", "Enable filter", "Whether to enable filter.", PARAM_FLAGS, false),
                EnableMulti: GObject.ParamSpec.boolean("enable-multi", "Enable multi", "Whether to enable multi selection.", PARAM_FLAGS, false),
                EnableSort: GObject.ParamSpec.boolean("enable-sort", "Enable sort", "Whether to enable sort.", PARAM_FLAGS, false),
                Expression: Gtk.param_spec_expression("expression", "Expression", "The expression that gets evaluated to obtain strings from items.", PARAM_FLAGS),
                SearchMatchMode: GObject.ParamSpec.enum("search-match-mode", "Search match mode", "The search match mode of the model.", PARAM_FLAGS, Gtk.StringFilterMatchMode, Gtk.StringFilterMatchMode.PREFIX)
            },
            Signals: {
                "selection-changed": {
                    param_types: []
                }
            }
        }, this);
    }

    constructor(props: ConstructorProperties) {
        super(props);
    }

    get collection() { return this._collection; }
    set collection(collection: ItemCollection<Model> | null) {
        if (this._collection === collection) return;

        if (collection === null) {
            this.expression = null;
            this.items = [];
        }
        else {
            this.expression = collection.expression;
            this.items = collection.toArray();
            collection.connect("items-changed", () => this.items = collection.toArray());
        }

        this._collection = collection;

        this.notify("collection");
    }

    get enableFilter() { return this._enableFilter; }
    set enableFilter(enableFilter: boolean) {
        if (this._enableFilter === enableFilter) return;

        if (enableFilter) {
            this._listBox.set_filter_func(listBoxRow => {
                if (!(listBoxRow instanceof ListBoxRowItem)) return true;

                if (this._toggleButtonSelected.get_active() && !this.selection.has(listBoxRow.item)) return false

                const label = (listBoxRow.get_child() as Gtk.Box).get_last_child() as Gtk.Label;
                if (!label.get_text().toLowerCase().includes(this._searchEntry.get_text().toLowerCase())) return false;

                return true;
            })
        }
        else this._listBox.set_filter_func(null);

        this._listBox.invalidate_filter();

        this._enableFilter = enableFilter;
        this.notify("enable-filter");
    }

    get enableMulti() { return this._enableMulti; }
    set enableMulti(enableMulti: boolean) {
        if (this._enableMulti === enableMulti) return;

        if (!enableMulti) {
            this.clearSelection();
            this.onSelectionChanged();
        }

        this._enableMulti = enableMulti;
        this.notify("enable-multi");
    }

    get enableSort() { return this._enableSort; }
    set enableSort(enableSort: boolean) {
        if (this._enableSort === enableSort) return;

        if (enableSort) {
            this._listBox.set_sort_func((listBoxRow1, listBoxRow2) => {
                if (!(listBoxRow1 instanceof ListBoxRowItem) || !(listBoxRow2 instanceof ListBoxRowItem)) return 0;

                const label1 = (listBoxRow1.get_child() as Gtk.Box).get_last_child() as Gtk.Label;
                const label2 = (listBoxRow2.get_child() as Gtk.Box).get_last_child() as Gtk.Label;
                const text1 = label1.get_text().toLowerCase();
                const text2 = label2.get_text().toLowerCase();

                return text1.localeCompare(text2);
            });
        }
        else this._listBox.set_sort_func(null);

        this._listBox.invalidate_sort();

        this._enableSort = enableSort;
        this.notify("enable-sort");
    }

    get expression() { return this._expression; }
    set expression(expression: Gtk.Expression | null) {
        if (this._expression === expression) return;

        this._expression = expression;

        this.notify("expression");
    }

    public get items() { return this._items; }
    public set items(items: GObject.Object[] | Gtk.StringObject[]) {
        this._items = items;

        this._listBox.remove_all();
        if (items === null) return;

        for (const item of items) {
            this._listBox.append(this.createListRowItem(item));
        }
    }

    get searchMatchMode() { return this._searchMatchMode; }
    set searchMatchMode(searchMatchMode: Gtk.StringFilterMatchMode) {
        if (this._searchMatchMode === searchMatchMode) return;

        this._searchMatchMode = searchMatchMode;
        this._listBox.invalidate_filter();
        this.notify("search-match-mode");
    }

    private createListRowItem(item: GObject.Object | Gtk.StringObject) {
        const listBoxRowItem = new ListBoxRowItem();

        let text = _("(Unknown)");
        if (typeof item === "string") text = item;
        else if (item instanceof Gtk.StringObject) text = item.get_string();
        else if (this.expression) {
            // @ts-expect-error For some reason it doesn't accept empty constructor but there is no error
            const expressionValue = new GObject.Value();

            // Get string from expression if user specified one
            if (this.expression.evaluate(item, expressionValue)) text = expressionValue.get_string() ?? text;
        }

        const box = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL, spacing: 5 });
        const label = new Gtk.Label({ xalign: 0, label: text });
        const image = new Gtk.Image({ icon_name: "object-select-symbolic", opacity: this.selection.has(item) ? 100 : 0 });

        box.append(image);
        box.append(label);
        listBoxRowItem.set_child(box);

        const gestureClick = new Gtk.GestureClick();
        gestureClick.connect("pressed", () => this.onListBoxRowClicked(listBoxRowItem));
        listBoxRowItem.add_controller(gestureClick);

        const eventControllerKey = new Gtk.EventControllerKey();
        eventControllerKey.connect("key-pressed", (eventControllerKey, keyval, keycode, state) => {
            if (keyval === Gdk.KEY_space || keyval === Gdk.KEY_Return) {
                this.onListBoxRowClicked(listBoxRowItem);
            }
        });
        listBoxRowItem.add_controller(eventControllerKey);


        listBoxRowItem.item = item;

        return listBoxRowItem;
    }

    public clearSelection() {
        for (const item of this.selection) {
            const listBoxRowItem = item[1];
            const icon = (listBoxRowItem.get_child() as Gtk.Box).get_first_child() as Gtk.Image;
            icon.set_opacity(0);
            this.selection.delete(item[0]);
        }
        this.onSelectionChanged();
        this.emit("selection-changed");
    }

    public getSelected(): GObject.Object | null {
        return this.getSelection()[0] || null;
    }

    public getSelection() {
        return Array.from(this.selection.keys());
    }

    public setSelection(indexes: number[] | number) {
        if (this.enableSort) throw new Error("`setSelection` is not compatible with `enable-sort`.");

        if (!Array.isArray(indexes)) indexes = [indexes];

        for (const index of indexes) {
            const listBoxRow = this._listBox.get_row_at_index(index);
            if (listBoxRow && listBoxRow instanceof ListBoxRowItem) this.onListBoxRowClicked(listBoxRow);
        }
    }

    public select(index: number) {
        const listBoxRow = this._listBox.get_row_at_index(index);
        if (listBoxRow && listBoxRow instanceof ListBoxRowItem) this.onListBoxRowClicked(listBoxRow);
    }

    public selectItem(item: GObject.Object) {
        for (let i = 0; i < this.items.length; i++) {
            const listBoxRow = this._listBox.get_row_at_index(i);
            if (listBoxRow === null || !(listBoxRow instanceof ListBoxRowItem)) return;

            if (listBoxRow.item === item) {
                this.onListBoxRowClicked(listBoxRow);
                break;
            }
        }
    }

    public selectItems(items: GObject.Object[]) {
        for (let i = 0; i < this.items.length; i++) {
            const listBoxRow = this._listBox.get_row_at_index(i);
            if (listBoxRow === null || !(listBoxRow instanceof ListBoxRowItem)) return;

            if (items.includes(listBoxRow.item)) this.onListBoxRowClicked(listBoxRow);
        }
    }

    public onListBoxRowClicked(listBoxRowItem: ListBoxRowItem) {
        const icon = (listBoxRowItem.get_child() as Gtk.Box).get_first_child() as Gtk.Image;
        const item = listBoxRowItem.item;

        if (this.selection.has(item)) {
            this.selection.delete(item);
            icon.set_opacity(0);
        }
        else {
            if (!this.enableMulti) this.clearSelection();

            this.selection.set(item, listBoxRowItem);
            icon.set_opacity(100);
        }

        if (!this.enableMulti) this._popover.popdown();

        this._listBox.invalidate_filter();
        this.onSelectionChanged();
        this.emit("selection-changed");
    }

    private onSelectionChanged() {
        if (this.selection.size > 0) {
            let items = [];
            for (const item of this.selection) {
                const listBoxRowItem = item[1];
                const label = (listBoxRowItem.get_child() as Gtk.Box).get_last_child() as Gtk.Label;
                items.push(label.get_text());
            }

            this._labelSelected.set_visible(true);
            this._labelSelected.set_label(items.join(", "));
            this._labelSelectionEmpty.set_visible(false);
        }
        else {
            this._labelSelected.set_visible(false);
            this._labelSelected.set_label("");
            this._labelSelectionEmpty.set_visible(true);
        }
    }

    private onPopoverClosed() {
        this._searchEntry.set_text("");
        this._toggleButtonOpen.set_active(false);
    }

    private onSearchChanged(searchEntry: Gtk.SearchEntry) {
        this._listBox.invalidate_filter();
    }

    private onStopSearch(searchEntry: Gtk.SearchEntry) {
        if (searchEntry.get_text()) {
            searchEntry.set_text("");
            this._listBox.invalidate_filter();
        }
        else this._popover.popdown();
    }

    private onToggleButtoOpenToggled() {
        if (this._toggleButtonOpen.get_active()) this._popover.popup();
        else this._popover.popdown();

        // Update popover width to be the same as button
        this._popover.set_property("width-request", this._toggleButtonOpen.get_allocated_width());

        // @ts-expect-error For some reason it doesn't accept empty constructor but there is no error
        const activeValue = new GObject.Value();
        activeValue.init(GObject.TYPE_INT);
        activeValue.set_int(this._toggleButtonOpen.get_active() ? 1 : 0);
        this.update_state([Gtk.AccessibleState.EXPANDED], [activeValue]);
    }

    private onToggleButtoSelectedToggled() {
        this._listBox.invalidate_filter();
    }
}