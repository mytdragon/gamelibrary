import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GObject from "gi://GObject";

export default class PreferencesWindow extends Adw.PreferencesWindow {
    private _switchFilterUsedItemsOnly!: Adw.SwitchRow;

    static {
        GObject.registerClass({
            GTypeName: "PreferencesWindow",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/preferences-window.ui",
            InternalChildren: [
                "switchFilterUsedItemsOnly"
            ]
        }, this);
    }

    constructor() {
        super();

        // setup settings and bind settings to inputs
        const settings = new Gio.Settings({ schema_id: "dev.mytdragon.GameLibrary" });
        settings.bind("filter-used-items-only", this._switchFilterUsedItemsOnly, "active", Gio.SettingsBindFlags.DEFAULT);
    }
}
