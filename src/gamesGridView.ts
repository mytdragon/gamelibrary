import Gdk from "gi://Gdk?version=4.0";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import Game from "./database/models/game.js";

import DropDownGroupBy from "./widgets/dropDownGroupBy.js";
import DropDownSortBy from "./widgets/dropDownSortBy.js";
import FilterView from "./filterView.js";
import GameCard from "./gameCard.js";

interface GameListItem extends Gtk.ListItem {
    signalHandlerButtonMain?: number;
    signalHandlerButtonMenu?: number;
    signalHandlerButtonPlay?: number;
}

export default class GamesGridView extends Gtk.GridView {
    private database = Database.getInstance();

    private customFilter: Gtk.CustomFilter;
    private customSorter: Gtk.CustomSorter;
    
    private dropDownGroupBy: DropDownGroupBy | null = null;
    private dropDownSortBy: DropDownSortBy | null = null;
    private filterView: FilterView | null = null;
    private searchEntry: Gtk.SearchEntry | null = null;

    public selection: Gtk.SingleSelection;

    static {
        GObject.registerClass({
            GTypeName: "GamesGridView"
        }, this);
    }

    constructor() {
        super();

        this.set_name("GamesGridView");

        const settings = new Gio.Settings({ schema_id: "dev.mytdragon.GameLibrary" });

        // Custom filter that use the hidden settings, filter view filters and search bar
        this.customFilter = new Gtk.CustomFilter();
        this.customFilter.set_filter_func(item => {
            if (!(item instanceof Game)) return true;

            if (!settings.get_boolean("show-hidden") && item.hidden === true) return false;

            if (this.filterView && !this.filterView.check(item)) return false;

            if (this.searchEntry && !item.name.toLowerCase().includes(this.searchEntry.get_text().toLowerCase())) return false;

            return true;
        });

        // Custom sorter that take the sort by and direction value from DropDownSortBy
        this.customSorter = new Gtk.CustomSorter();
        this.customSorter.set_sort_func((a: Game, b: Game) => {
            if (this.dropDownSortBy === null) return a.name.localeCompare(b.name);
            else return this.dropDownSortBy.compare(a, b);
        });

        // Connect to different events that would require to re-filter and re-sort
        settings.connect("changed", (_, key) => {
            if (key === "show-hidden") this.customFilter.emit("changed", Gtk.FilterChange.DIFFERENT);
        });

        this.database.gamesCollection.connect("item-updated", () => {
            this.customFilter.emit("changed", Gtk.FilterChange.DIFFERENT);
            this.customSorter.emit("changed", Gtk.SorterChange.DIFFERENT);
        });

        // Create the models in correct order
        const filterListModel = new Gtk.FilterListModel({ model: this.database.gamesCollection, filter: this.customFilter });
        const sortListModel = new Gtk.SortListModel({ model: filterListModel, sorter: this.customSorter });
        this.selection = new Gtk.SingleSelection({ model: sortListModel });

        // View items
        const factory = new Gtk.SignalListItemFactory();
        factory.connect("setup", (_, listItem: GameListItem) => {
            const gameCard = new GameCard(null);
            listItem.set_child(gameCard);
        });
        factory.connect("bind", (_, listItem: GameListItem) => {
            const gameCard = listItem.get_child() as GameCard;
            const game = listItem.get_item() as Game;
            
            gameCard.game = game;

            // Events on the card that would select the item
            listItem.signalHandlerButtonMain = gameCard._mainButton.connect("clicked", () => {
                if (this.selection.get_selected() !== listItem.get_position()) {
                    this.selection.select_item(listItem.get_position(), true);
                }
                this.emit("activate", listItem.get_position());
            });
            listItem.signalHandlerButtonMenu = gameCard._gestureClickMenu.connect("pressed", (gestureClick) => {
                if (gestureClick.get_current_button() === Gdk.BUTTON_PRIMARY) {
                    this.selection.select_item(listItem.get_position(), true);
                }
            });
            listItem.signalHandlerButtonPlay = gameCard._buttonPlay.connect("clicked", () => this.selection.select_item(listItem.get_position(), true));
        });
        factory.connect("unbind", (_, listItem: GameListItem) => {
            const gameCard = listItem.get_child() as GameCard;
            gameCard.game = null;
            gameCard.cancellable.cancel();

            if (listItem.signalHandlerButtonMain) GObject.signal_handler_disconnect(gameCard._mainButton, listItem.signalHandlerButtonMain)
            if (listItem.signalHandlerButtonMenu) GObject.signal_handler_disconnect(gameCard._gestureClickMenu, listItem.signalHandlerButtonMenu)
            if (listItem.signalHandlerButtonPlay) GObject.signal_handler_disconnect(gameCard._buttonPlay, listItem.signalHandlerButtonPlay)
        });

        this.connect("activate", (_, position) => this.selection.select_item(position, true));

        this.set_model(this.selection);
        this.set_factory(factory);
    }

    public setDropDownGroupBy(dropDownGroupBy: DropDownGroupBy) {
        this.dropDownGroupBy = dropDownGroupBy;
        this.dropDownGroupBy.connect("changed", (_, field) => {
            console.log(field);
        });
    }

    public setDropDownSortBy(dropDownSortBy: DropDownSortBy) {
        this.dropDownSortBy = dropDownSortBy;
        this.dropDownSortBy.connect("sort-field-changed", () => this.customSorter.emit("changed", Gtk.SorterChange.DIFFERENT));
        this.dropDownSortBy.connect("order-changed", () => this.customSorter.emit("changed", Gtk.SorterChange.DIFFERENT));
    }

    public setFilterView(filterView: FilterView) {
        this.filterView = filterView;
        this.filterView.connect("changed", () => this.customFilter.emit("changed", Gtk.FilterChange.DIFFERENT));
    }

    public setSearchEntry(searchEntry: Gtk.SearchEntry) {
        this.searchEntry = searchEntry;
        this.searchEntry.connect("search-changed", () => this.customFilter.emit("changed", Gtk.FilterChange.DIFFERENT));
    }
}