import Gio from "gi://Gio";
import GLib from "gi://GLib";
import GObject from "gi://GObject";

import * as Fs from "../utils/fs.js";

import Model from "./models/model.js";
import {
    AgeRating,
    CompletionStatus,
    Developer,
    Game,
    GameMode,
    Genre,
    Keyword,
    Platform,
    PlayerPerspective,
    Publisher,
    Region,
    Serie,
    Source,
    Theme
} from "./models/index.js";

import ItemCollection from "./collections/itemCollection.js";
import {
    AgeRatingsCollection,
    CompletionStatusesCollection,
    DevelopersCollection,
    GamesCollection,
    GameModesCollection,
    GenresCollection,
    KeywordsCollection,
    PlatformsCollection,
    PlayerPerspectivesCollection,
    PublishersCollection,
    RegionsCollection,
    SeriesCollection,
    SourcesCollection,
    ThemesCollection
} from "./collections/index.js";

interface DatabaseSettings {
    version: number;
};

export default class Database extends GObject.Object {
    private static instance: Database;

    private readonly DATABASE_VERSION_CURRENT = 1;
    private readonly DEFAULT_COMPLETION_STATUSES = [
        "Beaten",
        "Completed",
        "Dropped",
        "Not played",
        "Paused",
        "Planned",
        "Played",
        "Playing"
    ];

    private databasePath = `${GLib.get_user_data_dir()}/${pkg.name}/library`;
    private ageRatingsPath = `${this.databasePath}/age_ratings.json`;
    private completionStatusesPath = `${this.databasePath}/completion_statuses.json`;
    private databaseSettingsPath = `${this.databasePath}/database_settings.json`;
    private developersPath = `${this.databasePath}/developers.json`;
    private filesPath = `${this.databasePath}/files`;
    private gameModesPath = `${this.databasePath}/game_modes.json`;
    private gamesPath = `${this.databasePath}/games.json`;
    private genresPath = `${this.databasePath}/genres.json`;
    private keywordsPath = `${this.databasePath}/keywords.json`;
    private platformsPath = `${this.databasePath}/platforms.json`;
    private playerPerspectivesPath = `${this.databasePath}/player_perspectives.json`;
    private publishersPath = `${this.databasePath}/publishers.json`;
    private regionsPath = `${this.databasePath}/regions.json`;
    private seriesPath = `${this.databasePath}/series.json`;
    private sourcesPath = `${this.databasePath}/sources.json`;
    private themesPath = `${this.databasePath}/themes.json`;

    public isOpen = false;
    private databaseSettings?: DatabaseSettings;

    public ageRatingsCollection = new AgeRatingsCollection({ database: this, storagePath: this.ageRatingsPath });
    public completionStatusesCollection = new CompletionStatusesCollection({ database: this, storagePath: this.completionStatusesPath });
    public developersCollection = new DevelopersCollection({ database: this, storagePath: this.developersPath });
    public gameModesCollection = new GameModesCollection({ database: this, storagePath: this.gameModesPath });
    public gamesCollection = new GamesCollection({ database: this, storagePath: this.gamesPath });
    public genresCollection = new GenresCollection({ database: this, storagePath: this.genresPath });
    public keywordsCollection = new KeywordsCollection({ database: this, storagePath: this.keywordsPath });
    public platformsCollection = new PlatformsCollection({ database: this, storagePath: this.platformsPath });
    public playerPerspectivesCollection = new PlayerPerspectivesCollection({ database: this, storagePath: this.playerPerspectivesPath });
    public publishersCollection = new PublishersCollection({ database: this, storagePath: this.publishersPath });
    public regionsCollection = new RegionsCollection({ database: this, storagePath: this.regionsPath });
    public seriesCollection = new SeriesCollection({ database: this, storagePath: this.seriesPath });
    public sourcesCollection = new SourcesCollection({ database: this, storagePath: this.sourcesPath });
    public themesCollection = new ThemesCollection({ database: this, storagePath: this.themesPath });

    public collectionsMap = new Map<typeof Model, ItemCollection<Model>>();

    public ageRatingsUsed: string[] = [];
    public completionStatusesUsed: string[] = [];
    public developersUsed: string[] = [];
    public gameModesUsed: string[] = [];
    public genresUsed: string[] = [];
    public keywordsUsed: string[] = [];
    public platformsUsed: string[] = [];
    public playerPerspectivesUsed: string[] = [];
    public publishersUsed: string[] = [];
    public regionsUsed: string[] = [];
    public releaseYearsUsed: number[] = [];
    public seriesUsed: string[] = [];
    public sourcesUsed: string[] = [];
    public themesUsed: string[] = [];

    static {
        GObject.registerClass({
            GTypeName: "Database",
            Signals: {
                "used-items-changed": {
                    param_types: []
                }
            }
        }, this);
    }

    static getInstance(): Database {
        if (!Database.instance) {
            Database.instance = new Database();
        }

        return Database.instance;
    }

    private loadCollections() {
        /**
         * Load all the data into the associated collection
         */
        this.ageRatingsCollection.initializeCollection();
        this.completionStatusesCollection.initializeCollection();
        this.developersCollection.initializeCollection();
        this.gameModesCollection.initializeCollection();
        this.gamesCollection.initializeCollection();
        this.genresCollection.initializeCollection();
        this.keywordsCollection.initializeCollection();
        this.platformsCollection.initializeCollection();
        this.playerPerspectivesCollection.initializeCollection();
        this.publishersCollection.initializeCollection();
        this.regionsCollection.initializeCollection();
        this.seriesCollection.initializeCollection();
        this.sourcesCollection.initializeCollection();
        this.themesCollection.initializeCollection();

        /**
         * Set collection in map by Model type
         */
        this.collectionsMap.set(AgeRating, this.ageRatingsCollection);
        this.collectionsMap.set(CompletionStatus, this.completionStatusesCollection);
        this.collectionsMap.set(Developer, this.developersCollection);
        this.collectionsMap.set(GameMode, this.gameModesCollection);
        this.collectionsMap.set(Game, this.gamesCollection);
        this.collectionsMap.set(Genre, this.genresCollection);
        this.collectionsMap.set(Keyword, this.keywordsCollection);
        this.collectionsMap.set(Platform, this.platformsCollection);
        this.collectionsMap.set(PlayerPerspective, this.playerPerspectivesCollection);
        this.collectionsMap.set(Publisher, this.publishersCollection);
        this.collectionsMap.set(Region, this.regionsCollection);
        this.collectionsMap.set(Serie, this.seriesCollection);
        this.collectionsMap.set(Source, this.sourcesCollection);
        this.collectionsMap.set(Theme, this.themesCollection);
    }

    private loadUsedItems() {
        /**
         * Loop into games and add in arrays the used arrays the missing id 
         */
        for (let game of this.gamesCollection) this.updateUsedItems(game);
    }

    private updateUsedItems(game: Game) {
        /**
         * Currently, it won't check for used items that were removed.
         * It would require to check the used items that are not included in every games.
         */
        if (game.completionStatusId && !this.completionStatusesUsed.includes(game.completionStatusId)) this.completionStatusesUsed.push(game.completionStatusId);
        this.ageRatingsUsed = this.ageRatingsUsed.concat(game.ageRatingIds.filter(id => !this.ageRatingsUsed.includes(id)));
        this.developersUsed = this.developersUsed.concat(game.developerIds.filter(id => !this.developersUsed.includes(id)));
        this.gameModesUsed = this.gameModesUsed.concat(game.gameModeIds.filter(id => !this.gameModesUsed.includes(id)));
        this.genresUsed = this.genresUsed.concat(game.genreIds.filter(id => !this.genresUsed.includes(id)));
        this.keywordsUsed = this.keywordsUsed.concat(game.keywordIds.filter(id => !this.keywordsUsed.includes(id)));
        this.platformsUsed = this.platformsUsed.concat(game.platformIds.filter(id => !this.platformsUsed.includes(id)));
        this.playerPerspectivesUsed = this.playerPerspectivesUsed.concat(game.playerPerspectiveIds.filter(id => !this.playerPerspectivesUsed.includes(id)));
        this.publishersUsed = this.publishersUsed.concat(game.publisherIds.filter(id => !this.publishersUsed.includes(id)));
        this.regionsUsed = this.regionsUsed.concat(game.regionIds.filter(id => !this.regionsUsed.includes(id)));
        this.seriesUsed = this.seriesUsed.concat(game.serieIds.filter(id => !this.seriesUsed.includes(id)));
        if (game.sourceId && !this.sourcesUsed.includes(game.sourceId)) this.sourcesUsed.push(game.sourceId);
        this.themesUsed = this.themesUsed.concat(game.themeIds.filter(id => !this.themesUsed.includes(id)));

        const releaseYear = game.releaseDate?.getFullYear();
        if (releaseYear && !this.releaseYearsUsed.includes(releaseYear)) this.releaseYearsUsed.push(releaseYear);

        this.emit("used-items-changed");
    }

    public openDatabase() {
        const dbExists = Fs.check(this.databaseSettingsPath);

        if (!dbExists) {
            Fs.ensureDirectory(this.databasePath);
            Fs.ensureDirectory(this.filesPath);

            this.databaseSettings = { version: this.DATABASE_VERSION_CURRENT };
            Fs.writeFile(JSON.stringify(this.databaseSettings), this.databaseSettingsPath);
        }
        else {
            this.databaseSettings = JSON.parse(Fs.readTextFileFromPath(this.databaseSettingsPath)) as DatabaseSettings;

            if (this.databaseSettings.version > this.DATABASE_VERSION_CURRENT) throw new Error("Database version unsupported.");
            else if (this.databaseSettings.version < this.DATABASE_VERSION_CURRENT) throw new Error("Database must be migrated to a newer version.");
        }

        // this.generateSampleData();

        this.loadCollections();
        this.loadUsedItems();

        this.gamesCollection.connect("item-added", (_, game) => this.updateUsedItems(game));
        this.gamesCollection.connect("item-updated", (_, game) => this.updateUsedItems(game));

        if (!dbExists) {
            this.completionStatusesCollection.addManyFromName(this.DEFAULT_COMPLETION_STATUSES);
        }

        this.isOpen = true;
    }

    public checkDb() {
        if (!this.isOpen) throw new Error("Database is not opened.");
    }

    public addFile(file: Gio.File, parentId: string) {
        this.checkDb();

        const uuid = GLib.uuid_string_random();
        const dbPath = `${parentId}/${uuid}.jpg`;
        const path = `${this.filesPath}/${dbPath}`;

        Fs.ensureDirectory(GLib.path_get_dirname(path));
        if (!Fs.copyFile(file, path)) {
            console.error("Failed to add file to database");
            return null;
        }

        return dbPath;
    }

    public getFile(dbPath: string) {
        this.checkDb();

        const path = `${this.filesPath}/${dbPath}`;
        return Fs.getFile(path);
    }

    public getFileAsyncAsImage(dbPath: string, cancellable: Gio.Cancellable) {
        this.checkDb();

        const path = `${this.filesPath}/${dbPath}`;
        return Fs.getFileAsyncAsImage(path, cancellable);
    }
    
    public trashFile(dbPath: string) {
        this.checkDb();

        const path = `${this.filesPath}/${dbPath}`;
        const result = Fs.trashFile(path);

        if (!result) {
            console.error("Failed to trash file from database.");
            return false;
        }

        const dirPath = GLib.path_get_dirname(path);
        const iterr = Fs.iter(dirPath);
        const fileInfos = iterr.next_file(null);
        if (fileInfos === null) return Fs.trashFile(dirPath);

        return true;
    }

    private generateSampleData() {
        let games = [];
        const statuses = ["99dbcd8b-5ee4-44c3-bc9e-c28f97bfe312", "3d9d5845-94b6-4787-8258-a2ac4854fd48", "9faf1785-d2e7-423a-b286-1222483af40c", "fd3e3868-a8d5-4428-bf1e-9edb884e2390"];
        for (let i = 0; i < 500; i++) {
            let status = Math.floor(Math.random() * statuses.length);
            games.push(new Game({
                id: GLib.uuid_string_random(),
                name: `Test ${i}`,
                ageRatingIds: [],
                backgroundImage: null,
                coverImage: "c3634a56-5ce0-464f-ba29-dd6ffe668068/aaf2ebe2-78c6-4b8e-ba22-e3b9faf7f9f6.jpg",
                completionStatusId: statuses[status],
                communityRating: null,
                criticRating: null,
                userRating: null,
                created: new Date(),
                modified: new Date(),
                description: null,
                developerIds: [],
                platformIds: [],
                playerPerspectiveIds: [],
                gameModeIds: [],
                genreIds: [],
                keywordIds: [],
                themeIds: [],
                hidden: false,
                lastPlayed: null,
                links: [],
                notes: null,
                playCount: 0,
                playTime: 0,
                publisherIds: [],
                regionIds: [],
                releaseDate: null,
                serieIds: [],
                sourceId: null,
                version: null
            }));
        }
        Fs.writeFile(JSON.stringify(games), this.gamesPath);
    }
};