import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { GameMode } from "../models/index.js";

export default class GameModesCollection extends ItemCollection<GameMode> {
    static {
        GObject.registerClass({
            GTypeName: "GameModesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: GameMode, ...properties });
    }
}