import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { CompletionStatus } from "../models/index.js";

export default class ComnpletionStatusesCollection extends ItemCollection<CompletionStatus> {
    static {
        GObject.registerClass({
            GTypeName: "ComnpletionStatusesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: CompletionStatus, ...properties });
    }
}