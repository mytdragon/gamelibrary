import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Source } from "../models/index.js";

export default class SourcesCollection extends ItemCollection<Source> {
    static {
        GObject.registerClass({
            GTypeName: "SourcesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Source, ...properties });
    }
}