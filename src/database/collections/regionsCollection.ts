import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Region } from "../models/index.js";

export default class RegionsCollection extends ItemCollection<Region> {
    static {
        GObject.registerClass({
            GTypeName: "RegionsCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Region, ...properties });
    }
}