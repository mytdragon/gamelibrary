import AgeRatingsCollection from "./ageRatingsCollection.js";
import CompletionStatusesCollection from "./completionStatusesCollection.js";
import DevelopersCollection from "./developersCollection.js";
import GameModesCollection from "./gameModesCollection.js";
import GamesCollection from "./gamesCollection.js";
import GenresCollection from "./genresCollection.js";
import KeywordsCollection from "./keywordsCollection.js";
import PlatformsCollection from "./platformsCollection.js";
import PlayerPerspectivesCollection from "./playerPerspectivesCollection.js";
import PublishersCollection from "./publishersCollection.js";
import RegionsCollection from "./regionsCollection.js";
import SeriesCollection from "./seriesCollection.js";
import SourcesCollection from "./sourcesCollection.js";
import ThemesCollection from "./themesCollection.js";

export {
    AgeRatingsCollection,
    CompletionStatusesCollection,
    DevelopersCollection,
    GameModesCollection,
    GamesCollection,
    GenresCollection,
    KeywordsCollection,
    PlatformsCollection,
    PlayerPerspectivesCollection,
    PublishersCollection,
    RegionsCollection,
    SeriesCollection,
    SourcesCollection,
    ThemesCollection
}