import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import Game, { GroupField } from "../models/game.js";
import Model from "../models/model.js";

export default class GamesCollection extends ItemCollection<Game> {
    static {
        GObject.registerClass({
            GTypeName: "GamesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Game, ...properties });
    }

    public remove(game: Game) {
        if (game.backgroundImage) this.database.trashFile(game.backgroundImage);
        if (game.coverImage) this.database.trashFile(game.coverImage);

        super.remove(game);
    }

    public groupBy(key: GroupField) {
        const groupedGames = new Map<Model | string | null, Game[]>();
        for (const game of this) {
            // Retrieve the groups of the game for the field we want to group with
            let values = [];
            switch (key) {
                case "ageRatingIds":
                    if (game.ageRatingIds.length > 0) {
                        for (const id of game.ageRatingIds) {
                            const item = this.database.ageRatingsCollection.getById(id);
                            if (item) values.push(item);
                        };
                    }
                    break;
                
                case "completionStatusId":
                    if (game.completionStatusId) {
                        const item = this.database.completionStatusesCollection.getById(game.completionStatusId);
                        if (item) values.push(item);
                    }                    
                    break;
                
                default:
                    throw new Error("Unsupported group key.");
            }

            // If no value, we group with the null (none)
            if (values.length === 0) {
                if (groupedGames.has(null)) groupedGames.get(null)!.push(game);
                else groupedGames.set(null, [game]);
            }
            else {
                for (const item of values) {
                    if (item !== null) {
                        if (groupedGames.has(item)) groupedGames.get(item)!.push(game);
                        else groupedGames.set(item, [game]);
                    }
                }
            }
        }
        
        return groupedGames;
    }
}