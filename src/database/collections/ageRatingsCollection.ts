import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { AgeRating } from "../models/index.js";

export default class AgeRatingsCollection extends ItemCollection<AgeRating> {
    static {
        GObject.registerClass({
            GTypeName: "AgeRatingsCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: AgeRating, ...properties });
    }
}