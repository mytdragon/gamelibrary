import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Developer } from "../models/index.js";

export default class DevelopersCollection extends ItemCollection<Developer> {
    static {
        GObject.registerClass({
            GTypeName: "DevelopersCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Developer, ...properties });
    }
}