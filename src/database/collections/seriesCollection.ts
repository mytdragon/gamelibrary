import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Serie } from "../models/index.js";

export default class SeriesCollection extends ItemCollection<Serie> {
    static {
        GObject.registerClass({
            GTypeName: "SeriesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Serie, ...properties });
    }
}