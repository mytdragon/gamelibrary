import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Theme } from "../models/index.js";

export default class ThemesCollection extends ItemCollection<Theme> {
    static {
        GObject.registerClass({
            GTypeName: "ThemesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Theme, ...properties });
    }
}