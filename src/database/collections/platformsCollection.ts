import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Platform } from "../models/index.js";

export default class PlatformsCollection extends ItemCollection<Platform> {
    static {
        GObject.registerClass({
            GTypeName: "PlatformsCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Platform, ...properties });
    }
}