import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Keyword } from "../models/index.js";

export default class KeywordsCollection extends ItemCollection<Keyword> {
    static {
        GObject.registerClass({
            GTypeName: "KeywordsCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Keyword, ...properties });
    }
}