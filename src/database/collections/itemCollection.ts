import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from "gi://GObject";
import Gtk from 'gi://Gtk?version=4.0';

import { getFile, readTextFile, writeFile } from "../../utils/fs.js";
import Database from '../database.js';
import Model from '../models/model.js';

export interface ConstructorProperties {
    database: Database;
    storagePath: string;
    model: typeof Model;
}

export default class ItemCollection<TItem extends Model> extends GObject.Object implements Gio.ListModel<TItem> {
    public readonly database: Database;
    private storagePath: string;

    public model: typeof Model;
    public itemType: GObject.GType;
    public expression: Gtk.PropertyExpression;

    private items: TItem[] = [];

    static {
        GObject.registerClass({
            GTypeName: "ItemCollection",
            Implements: [Gio.ListModel],
            Signals: {
                "item-added": {
                    param_types: [GObject.TYPE_OBJECT]
                },
                "item-updated": {
                    param_types: [GObject.TYPE_OBJECT]
                },
                "item-removed": {
                    param_types: [GObject.TYPE_OBJECT]
                }
            }
        }, this);
    }

    constructor(properties: ConstructorProperties) {
        super();

        this.database = properties.database;
        this.storagePath = properties.storagePath;

        this.model = properties.model;
        this.itemType = properties.model.$gtype;
        this.expression = new Gtk.PropertyExpression(this.model, null, "name");
    }

    [Symbol.iterator]() {
        return this.items[Symbol.iterator]();
    }

    public count() {
        return this.items.length;
    }

    public get(position: number) {
        return this.items[position] ?? null;
    }

    public getById(id: string) {
        return this.items.find(item => item.id === id) ?? null;
    }

    public add(item: TItem) {
        if (this.items.some(i => i.id === item.id)) throw new Error(`Item ${item.id} already exists.`);
        this.items.push(item);

        this.save();
        this.emit("item-added", item);
        this.emit("items-changed", this.count() - 1, 0, 1);

        return item;
    }

    public addFromName(name: string) {
        const item = new this.model({
            id: GLib.uuid_string_random(),
            name: name
        }) as TItem;

        return this.add(item);
    }

    public addMany(items: TItem[]) {
        for (const item of items) {
            if (this.items.some(i => i.id === item.id)) throw new Error(`Item with id ${item.id} already exists.`);
            this.items.push(item);
        }

        this.save();
        this.emit("items-changed");
        this.emit("items-changed", this.count() - 1 - items.length, 0, items.length);
    }

    public addManyFromName(names: string[]) {
        for (const name of names) {
            if (this.items.some(i => i.name === name)) throw new Error(`Item with name ${name} already exists.`);

            const item = new this.model({
                id: GLib.uuid_string_random(),
                name: name
            }) as TItem;

            this.items.push(item);
        }

        this.save();
        this.emit("items-changed", this.count() - 1 - names.length, 0, names.length);
    }

    public remove(item: TItem) {
        const index = this.items.findIndex(i => i.id === item.id);
        if (index === -1) throw new Error(`Item ${item.id} doesn't exists.`);

        this.items.splice(index, 1);

        this.save();
        this.emit("item-removed", item);
        this.emit("items-changed", index, 1, 0);
    }

    public update(item: TItem) {
        const index = this.items.findIndex(i => i.id === item.id);
        if (index === -1) throw new Error(`Item ${item.id} doesn't exists.`);

        this.items[index] = item;

        this.save();
        this.emit("item-updated", item);
        this.emit("items-changed", index, 0, 0);
    }

    public save() {
        const result = writeFile(JSON.stringify(this.items), this.storagePath);
        if (!result) throw new Error(`Cannot save ${this.constructor.name} to ${this.storagePath}.`);
    }

    public toArray() {
        return this.items;
    }

    public toJSON() {
        return this.items;
    }

    public initializeCollection() {
        // Create json file if doesn't exist
        const file = getFile(this.storagePath);
        if (!file) {
            writeFile(JSON.stringify([]), this.storagePath);
            return;
        }

        // Get file content and revive the JSON
        const data = JSON.parse(readTextFile(file), (key, value) => {
            const validDateRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;
            if (typeof value === "string" && validDateRegex.test(value)) {
                return new Date(value);
            }

            return value;
        });
        this.items = data.map((item: TItem) => {
            return new this.model(item);
        });
    }

    // These are require for the Gio.ListModel implementation
    public items_changed(position: number, removed: number, added: number) {}
    public get_item_type() { return this.itemType; }
    public vfunc_get_item_type() { return this.get_item_type() }
    public get_item(position: number) { return this.get(position); }
    public vfunc_get_item(position: number) { return this.get(position); }
    public get_n_items() { return this.count(); }
    public vfunc_get_n_items() { return this.count(); }
}