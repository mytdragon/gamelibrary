import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Genre } from "../models/index.js";

export default class GenresCollection extends ItemCollection<Genre> {
    static {
        GObject.registerClass({
            GTypeName: "GenresCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Genre, ...properties });
    }
}