import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { PlayerPerspective } from "../models/index.js";

export default class PlayerPerspectivesCollection extends ItemCollection<PlayerPerspective> {
    static {
        GObject.registerClass({
            GTypeName: "PlayerPerspectivesCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: PlayerPerspective, ...properties });
    }
}