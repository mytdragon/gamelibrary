import GObject from "gi://GObject";

import ItemCollection, { ConstructorProperties } from "./itemCollection.js";
import { Publisher } from "../models/index.js";

export default class PublishersCollection extends ItemCollection<Publisher> {
    static {
        GObject.registerClass({
            GTypeName: "PublishersCollection"
        }, this);
    }

    constructor(properties: Omit<ConstructorProperties, "model">) {
        super({ model: Publisher, ...properties });
    }
}