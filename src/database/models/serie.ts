import GObject from "gi://GObject";

import Model from "./model.js";

export default class Serie extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Serie"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}