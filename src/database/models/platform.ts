import GObject from "gi://GObject";

import Model from "./model.js";

export default class Platform extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Platform"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}