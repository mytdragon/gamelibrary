import GObject from "gi://GObject";

import Model from "./model.js";

export default class AgeRating extends Model {
    static {
        GObject.registerClass({
            GTypeName: "AgeRating"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}