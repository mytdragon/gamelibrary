import GObject from "gi://GObject";

import Model from "./model.js";

export default class Theme extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Theme"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}