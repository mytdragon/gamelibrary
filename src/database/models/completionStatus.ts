import GObject from "gi://GObject";

import Model from "./model.js";

export default class CompletionStatus extends Model {
    static {
        GObject.registerClass({
            GTypeName: "CompletionStatus"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}