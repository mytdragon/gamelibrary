import GObject from "gi://GObject";

import Model from "./model.js";

export default class Genre extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Genre"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}