import GObject from "gi://GObject";

import Model from "./model.js";

export default class Developer extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Developer"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}