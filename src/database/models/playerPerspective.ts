import GObject from "gi://GObject";

import Model from "./model.js";

export default class PlayerPerspective extends Model {
    static {
        GObject.registerClass({
            GTypeName: "PlayerPerspective"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}