import GObject from "gi://GObject";

import Model, { ConstructorProperties } from "./model.js";

export type Link = {
    title: string;
    url: string;
};

export interface GameData extends ConstructorProperties {
    ageRatingIds: string[];
    backgroundImage: string | null;
    completionStatusId: string | null;
    communityRating: number | null;
    coverImage: string | null;
    criticRating: number | null;
    created: Date;
    description: string | null;
    developerIds: string[];
    gameModeIds: string[];
    genreIds: string[];
    hidden: boolean;
    keywordIds: string[];
    lastPlayed: Date | null;
    links: Link[];
    modified: Date;
    notes: string | null;
    platformIds: string[];
    playCount: number;
    playTime: number;
    playerPerspectiveIds: string[];
    publisherIds: string[];
    regionIds: string[];
    releaseDate: Date | null;
    serieIds: string[];
    sourceId: string | null;
    themeIds: string[];
    userRating: number | null;
    version: string | null;
}

export type GroupField = keyof Pick<Game, "ageRatingIds" | "completionStatusId">;

export default class Game extends Model {
    public ageRatingIds: string[];
    public backgroundImage: string | null;
    public communityRating: number | null;
    public completionStatusId: string | null;
    public coverImage: string | null;
    public criticRating: number | null;
    public created: Date;
    public description: string | null;
    public developerIds: string[];
    public gameModeIds: string[];
    public genreIds: string[];
    public hidden: boolean;
    public keywordIds: string[];
    public lastPlayed: Date | null;
    public links: Link[];
    public modified: Date;
    public notes: string | null;
    public platformIds: string[];
    public playTime: number;
    public playCount: number;
    public playerPerspectiveIds: string[];
    public publisherIds: string[];
    public regionIds: string[];
    public releaseDate: Date | null;
    public serieIds: string[];
    public sourceId: string | null;
    public themeIds: string[];
    public userRating: number | null;
    public version: string | null;

    static {
        GObject.registerClass({
            GTypeName: "Game"
        }, this);
    }

    constructor(properties: GameData) {
        super({ id: properties.id, name: properties.name });
        
        this.ageRatingIds = properties.ageRatingIds || [];
        this.backgroundImage = properties.backgroundImage || null;
        this.completionStatusId = properties.completionStatusId || null;
        this.communityRating = properties.communityRating || null;
        this.coverImage = properties.coverImage || null;
        this.criticRating = properties.criticRating || null;
        this.created = properties.created;
        this.description = properties.description || null;
        this.developerIds = properties.developerIds || [];
        this.gameModeIds = properties.gameModeIds || [];
        this.genreIds = properties.genreIds || [];
        this.hidden = properties.hidden || false;
        this.keywordIds = properties.keywordIds || [];
        this.lastPlayed = properties.lastPlayed || null;
        this.links = properties.links || [];
        this.modified = properties.modified;
        this.modified = properties.modified;
        this.notes = properties.notes || null;
        this.platformIds = properties.platformIds || [];
        this.playCount = properties.playCount || 0;
        this.playTime = properties.playTime || 0;
        this.playerPerspectiveIds = properties.playerPerspectiveIds || [];
        this.publisherIds = properties.publisherIds || [];
        this.regionIds = properties.regionIds || [];
        this.releaseDate = properties.releaseDate || null;
        this.serieIds = properties.serieIds || [];
        this.sourceId = properties.sourceId || null;
        this.themeIds = properties.themeIds || [];
        this.userRating = properties.userRating || null;
        this.version = properties.version || null;
    }

    toJSON() {
        return {
            ...this,
            id: this.id,
            name: this.name
        };
    }
    
    public getFormattedPlayTime() {
        return this.playTime > 0 ? `${Math.round(this.playTime / 3600)} hours` : _("Not played");
    }
}