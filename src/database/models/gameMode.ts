import GObject from "gi://GObject";

import Model from "./model.js";

export default class GameMode extends Model {
    static {
        GObject.registerClass({
            GTypeName: "GameMode"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}