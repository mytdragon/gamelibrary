import GObject from "gi://GObject";

export type ConstructorProperties = {
    id: string;
    name: string;
};

export default class Model extends GObject.Object {
    public id!: string;
    public name!: string;

    static {
        GObject.registerClass({
            GTypeName: "Model",
            Properties: {
                Id: GObject.ParamSpec.string("id", "Id", "The id", GObject.ParamFlags.READWRITE, ""),
                Name: GObject.ParamSpec.string("name", "Name", "The name", GObject.ParamFlags.READWRITE, "")
            }
        }, this);
    }

    constructor(properties: ConstructorProperties) {
        super(properties);
    }
}