import GObject from "gi://GObject";

import Model from "./model.js";

export default class Region extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Region"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}