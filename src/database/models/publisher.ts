import GObject from "gi://GObject";

import Model from "./model.js";

export default class Publisher extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Publisher"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}