import Model from "./model.js";

import AgeRating from "./ageRating.js";
import CompletionStatus from "./completionStatus.js";
import Developer from "./developer.js";
import Game from "./game.js";
import GameMode from "./gameMode.js";
import Genre from "./genre.js";
import Keyword from "./keyword.js";
import Platform from "./platform.js";
import PlayerPerspective from "./playerPerspective.js";
import Publisher from "./publisher.js";
import Region from "./region.js";
import Serie from "./serie.js";
import Source from "./source.js";
import Theme from "./theme.js";

export {
    Model,

    AgeRating,
    CompletionStatus,
    Developer,
    Game,
    GameMode,
    Genre,
    Keyword,
    Platform,
    PlayerPerspective,
    Publisher,
    Region,
    Serie,
    Source,
    Theme
}