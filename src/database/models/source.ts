import GObject from "gi://GObject";

import Model from "./model.js";

export default class Source extends Model {
    static {
        GObject.registerClass({
            GTypeName: "Source"
        }, this);
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name
        };
    }
}