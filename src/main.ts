import { Application } from './application.js';

export function main(argv: string[]): number {
    // @ts-expect-error introspection metada doesn't indicate async functions but it exists.
    return new Application().runAsync(argv);
}