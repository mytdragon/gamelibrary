export const unionBy = (arrays: any[], key: any) => {
    const map = new Map();
    arrays.forEach(array => array.forEach((object: any) => map.set(object[key], object)));
    return [...map.values()];
}