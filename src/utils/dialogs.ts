import Gdk from "gi://Gdk?version=4.0";
import Gtk from "gi://Gtk?version=4.0";
import Adw from "gi://Adw";

export const confirmDialog = (window: Gtk.Window, heading: string, body: string, callbackAction: () => void, responseAppearance = Adw.ResponseAppearance.DESTRUCTIVE) => {
    const dialog = new Adw.MessageDialog({
        transientFor: window,
        heading: heading,
        body: body
    });
    dialog.add_response("no", _("No"));
    dialog.add_response("yes", _("Yes"));
    dialog.set_response_appearance("yes", responseAppearance);
    dialog.connect("response", (_, response) => {
        if (response === "yes") callbackAction();
    });
    dialog.present();
}

export const errorDialog = (window : Gtk.Window, heading: string, body: string) => {
    const dialog = new Adw.MessageDialog({
        transientFor: window,
        heading: heading,
        body: body
    });
    dialog.add_response("dismiss", _("OK"));
    dialog.present();
};


type EntryDialogProperties = {
    actionName?: string,
    callbackAction: (dialog: Adw.Window, value: string) => void,
    heading: string,
    placeholderText?: string,
    window: Gtk.Window    
};
export const entryDialog = (properties: EntryDialogProperties) => {
    const dialog = new Adw.Window({
        modal: true,
        resizable: false,
        transientFor: properties.window,
        cssClasses: ["messagedialog"]
    });

    const validate = () => {
        const text = entry.get_text();
        if (text === "") {
            errorDialog(dialog, _("Entry cannot be empty"), _("The entry cannot be empty."));
            return;
        }
        properties.callbackAction(dialog, text);
    }

    const eventControllerKey = new Gtk.EventControllerKey();
    eventControllerKey.connect("key-pressed", (eventControllerKey, keyval, keycode, state) => {
        if (keyval === Gdk.KEY_Escape) dialog.close();
    });
    dialog.add_controller(eventControllerKey);
    
    const vbox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: "message_area", cssClasses: ["message-area", "has-heading", "has-body"] });
    const entry = new Gtk.Entry({ placeholderText: properties.placeholderText });
    entry.connect("activate", () => validate())
    vbox.append(new Gtk.Label({ name: "heading_label", label: properties.heading, cssClasses: ["title-2"] }));
    vbox.append(entry);

    const responseBox = new Gtk.Box({ cssClasses: ["response-area"] });
    const cancelButton = new Gtk.Button({ label: _("Cancel"), halign: Gtk.Align.FILL, hexpand: true, cssClasses: ["flat"] });
    cancelButton.connect("clicked", () => dialog.close());
    const actionButton = new Gtk.Button({ label: properties.actionName || _("Save"), halign: Gtk.Align.FILL, hexpand: true, cssClasses: ["flat"] })
    actionButton.connect("clicked", () => validate());
    responseBox.append(cancelButton);
    responseBox.append(new Gtk.Separator);
    responseBox.append(actionButton);

    const containerBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
    containerBox.append(vbox);
    containerBox.append(new Gtk.Separator);
    containerBox.append(responseBox);

    const windowHandle = new Gtk.WindowHandle();
    windowHandle.set_child(containerBox);

    dialog.set_content(windowHandle);

    dialog.present();
}