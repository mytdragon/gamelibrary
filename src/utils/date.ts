import GLib from "gi://GLib";

export const formatDate = (date: Date, format: string) => {
    const glibDate = GLib.DateTime.new_from_unix_utc(date.getTime() / 1000);
    return glibDate.format(format) ?? date.toLocaleDateString("en-GB");
};

export const secondsToHms = (rawSeconds: number) => {
    rawSeconds = Number(rawSeconds);

    let hours = Math.floor(rawSeconds / 3600);
    let minutes = ("0" + Math.floor(rawSeconds % 3600 / 60)).slice(-2);
    let seconds = ("0" + Math.floor(rawSeconds % 3600 % 60)).slice(-2);

    return _("%hours%:%minutes%:%seconds%")
        .replace("%hours%", hours < 10 ? "0" + hours : hours.toString())
        .replace("%minutes%", minutes)
        .replace("%seconds%", seconds);
}