import Gdk from "gi://Gdk?version=4.0";
import GdkPixbuf from "gi://GdkPixbuf";
import Gio from "gi://Gio";
import GLib from "gi://GLib";

export const check = (path: string) => {
    return GLib.file_test(path, GLib.FileTest.EXISTS);
};

export const copyFile = (file: Gio.File, path: string) => {
    const targetFile = Gio.File.new_for_path(path);
    return file.copy(targetFile, Gio.FileCopyFlags.OVERWRITE, null, null);
}

export const dir = (path: string) => {
    return Gio.File.new_for_path(path);
};

export const ensureDirectory = (path: string) => {
    if (!check(path)) {
        Gio.File.new_for_path(path).make_directory_with_parents(null);
    }
};

export const getFile = (path: string) => {
    if (check(path)) {
        return Gio.File.new_for_path(path);
    }
    else return null;
};


export const getFileAsyncAsImage = (path: string, cancellable: Gio.Cancellable | null)  : Promise<Gdk.Texture> => {
    if (!check(path)) throw new Error(`File at "${path}" doesn't exist.`);

    const file = Gio.File.new_for_path(path);

    return new Promise((resolve, reject) => {
        file.load_contents_async(cancellable, (_, result) => {
            try {
                const [success, bytes] = file.load_contents_finish(result);
                if (success) {
                    const stream = Gio.MemoryInputStream.new_from_bytes(bytes);
                    const pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, cancellable);
                    const texture = Gdk.Texture.new_for_pixbuf(pixbuf);
                    resolve(texture);
                }
                else reject(Error(`Reading file at  "${path}" was unsuccessful.`));
            }
            catch (error) {
                // Cancelled error is expected
                if (!(error instanceof Gio.IOErrorEnum && error.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED))) reject(error);
            }
        });
    });
}

export const iter = (path: string) => {
    const directory = Gio.File.new_for_path(path);
    return directory.enumerate_children('standard::name', Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null);
};

export const readTextFileFromPath = (path: string) => {
    if (!check(path)) throw new Error(`File at "${path}" doesn't exist.`);

    const file = Gio.File.new_for_path(path);
    return readTextFile(file);
};

export const readTextFile = (file: Gio.FilePrototype) => {
    const [, bytes] = file.load_contents(null);
    return new TextDecoder().decode(bytes);
};

export const writeFile = (string: string, path: string) => {
    const file = Gio.File.new_for_path(path);

    const result = file.replace_contents(
        new TextEncoder().encode(string),
        null,
        false,
        Gio.FileCreateFlags.REPLACE_DESTINATION,
        null
    );
    return result[0];
};

export const trashFile = (path: string) => {
    if (!check(path)) return true;
    const file = Gio.File.new_for_path(path);
    return file.trash(null);
};
