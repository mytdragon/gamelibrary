import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GLib from "gi://GLib";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import Game from "./database/models/game.js";

import { confirmDialog } from "./utils/dialogs.js";

import "./widgets/dropDownGroupBy.js";
import DropDownGroupBy from "./widgets/dropDownGroupBy.js";

import "./widgets/dropDownSortBy.js";
import DropDownSortBy from "./widgets/dropDownSortBy.js";

import "./filterView.js";
import FilterView from "./filterView.js";

import "./gamesGridView.js";
import GamesGridView from "./gamesGridView.js";

import GameDetailsPage from "./gameDetailsPage.js";
import ManageGameWindow from "./manageGameWindow.js";

export default class GameLibraryWindow extends Adw.ApplicationWindow {
    private _dropDownGroupBy!: DropDownGroupBy;
    private _dropDownSortBy!: DropDownSortBy;
    private _filterView!: FilterView;
    private _gamesGridView!: GamesGridView;
    private _overlayLibrary!: Gtk.Overlay;
    private _menuButtonPrimary!: Gtk.MenuButton;
    private _navigationView!: Adw.NavigationView;
    private _searchBar!: Gtk.SearchBar;
    private _searchEntry!: Gtk.SearchEntry;
    private _statusPageEmpty!: Adw.StatusPage;
    private _statusPageNoResults!: Adw.StatusPage;
    private _toastOverlay!: Adw.ToastOverlay;
    private _toggleButtonFilter!: Gtk.ToggleButton;

    private settings!: Gio.Settings;
    private toggleHideGameAction!: Gio.SimpleAction;
    private showGameDetailsAction!: Gio.SimpleAction;

    private database = Database.getInstance();

    static {
        GObject.registerClass({
            GTypeName: "GameLibraryWindow",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/window.ui",
            InternalChildren: [
                "dropDownGroupBy",
                "dropDownSortBy",
                "filterView",
                "gamesGridView",
                "overlayLibrary",
                "menuButtonPrimary",
                "navigationView",
                "searchBar",
                "searchEntry",
                "statusPageEmpty",
                "statusPageNoResults",
                "toastOverlay",
                "toggleButtonFilter"
            ]
        }, this);
    }

    constructor(params?: Partial<Adw.ApplicationWindow.ConstructorProperties>) {
        super(params);

        this.setupSettings();
        this.setupActions();
        this.setupGamesGridView();
        
        this.setupUi();
        this.database.gamesCollection.connect("item-added", () => this.setupUi());
        this.database.gamesCollection.connect("item-removed", () => this.setupUi());
    }

    private setupSettings() {
        this.settings = new Gio.Settings({ schema_id: "dev.mytdragon.GameLibrary" });
        this.settings.bind("window-width", this, "default-width", Gio.SettingsBindFlags.DEFAULT);
        this.settings.bind("window-height", this, "default-height", Gio.SettingsBindFlags.DEFAULT);
    }

    private setupActions() {
        const addGameAction = new Gio.SimpleAction({ name: "add-game" });
        addGameAction.connect("activate", () => this.onAddGameAction());
        this.add_action(addGameAction);

        const editGameAction = new Gio.SimpleAction({ name: "edit-game" });
        editGameAction.connect("activate", () => this.onEditGameAction());
        this.add_action(editGameAction);

        const openMenuAction = new Gio.SimpleAction({ name: "open-menu" });
        openMenuAction.connect("activate", () => this.onOpenMenuAction());
        this.add_action(openMenuAction);

        this.showGameDetailsAction = new Gio.SimpleAction({ name: "show-game-details" });
        this.showGameDetailsAction.connect("activate", () => this.onShowGameDetailsAction());
        this.add_action(this.showGameDetailsAction);

        const showHiddenAction = this.settings.create_action("show-hidden");
        this.add_action(showHiddenAction);

        const toggleFilterViewAction = new Gio.SimpleAction({ name: "toggle-filter-view" });
        toggleFilterViewAction.connect("activate", () => this.onToggleFilterViewAction());
        this.add_action(toggleFilterViewAction);

        this.toggleHideGameAction = new Gio.SimpleAction({
            name: "toggle-hide-game",
            state: GLib.Variant.new_boolean(false)
        });
        this.toggleHideGameAction.connect("activate", () => this.onToggleHideGameAction());
        this.add_action(this.toggleHideGameAction);
        this.toggleHideGameAction = this.toggleHideGameAction;

        const toggleSearchAction = new Gio.SimpleAction({ name: "toggle-search" });
        toggleSearchAction.connect("activate", () => this.onToggleSearchAction());
        this.add_action(toggleSearchAction);

        const trashGameAction = new Gio.SimpleAction({ name: "trash-game" });
        trashGameAction.connect("activate", () => this.onTrashGameAction());
        this.add_action(trashGameAction);

        // Update state of hidden game (context menu) action if selection changes or item get updated
        this._gamesGridView.selection.connect("selection-changed", (selection) => {
            const game = selection.get_selected_item() as Game | null;
            if (game === null) return;

            this.toggleHideGameAction.set_state(GLib.Variant.new_boolean(game.hidden));
        });
        this.database.gamesCollection.connect("item-updated", (_, game) => {
            const gameSelected = this._gamesGridView.selection.get_selected_item() as Game | null;
            if (gameSelected === null) return;

            if (gameSelected.id === game.id) {
                this.toggleHideGameAction.set_state(GLib.Variant.new_boolean(game.hidden));
            }
        });
    }

    private setupUi() {
        if (this.database.gamesCollection.count() === 0) {
            this._overlayLibrary.add_overlay(this._statusPageEmpty);
        }
        else {
            if (this._overlayLibrary.get_last_child()?.get_name() === "StatusPageEmpty") this._overlayLibrary.remove_overlay(this._statusPageEmpty);
            if (this._overlayLibrary.get_last_child()?.get_name() === "StatusPageNoResults") this._overlayLibrary.remove_overlay(this._statusPageNoResults);
        }
    }

    private setupGamesGridView() {
        this._gamesGridView.setFilterView(this._filterView);
        this._gamesGridView.setSearchEntry(this._searchEntry);
        this._gamesGridView.setDropDownGroupBy(this._dropDownGroupBy);
        this._gamesGridView.setDropDownSortBy(this._dropDownSortBy);
        this._gamesGridView.connect("activate", () => this.showGameDetailsAction.activate(null));
    }

    public onShowGameDetailsAction() {
        const game = this._gamesGridView.selection.get_selected_item() as Game | null;
        if (game === null) return;
        this._navigationView.push(new GameDetailsPage(game));
    }

    private onAddGameAction() {
        const manageGameWindow = new ManageGameWindow(this);
        manageGameWindow.present();
    }

    private onEditGameAction() {
        const game = this._gamesGridView.selection.get_selected_item() as Game | null;
        if (game === null) return;
        const manageGameWindow = new ManageGameWindow(this, game);
        manageGameWindow.present();
    }

    private onOpenMenuAction() {
        this._menuButtonPrimary.popup();
    }

    private onToggleFilterViewAction() {
        this._toggleButtonFilter.set_active(!this._toggleButtonFilter.get_active());
    }

    private onToggleHideGameAction() {
        const game = this._gamesGridView.selection.get_selected_item() as Game | null;
        if (game === null) return;

        game.hidden = !game.hidden;
        this.database.gamesCollection.update(game);

        const message = game.hidden ? _("%game% hidden") : _("%game% unhidden");
        this._toastOverlay.add_toast(new Adw.Toast({
            title: message.replace("%game%", game.name)
        }));

        this.toggleHideGameAction.set_state(GLib.Variant.new_boolean(game.hidden));
    }

    private onToggleSearchAction() {
        const searchMode = this._searchBar.get_search_mode();
        this._searchBar.set_search_mode(!searchMode);

        if (!searchMode) this.set_focus(this._searchEntry);

        this._searchEntry.set_text("");
    }

    private onTrashGameAction() {
        const game = this._gamesGridView.selection.get_selected_item() as Game | null;
        if (game === null) return;

        confirmDialog(this, _("Trash game?"), _("Are you sure you want to trash %game%?").replace("%game%", game.name), () => {
            this._toastOverlay.add_toast(new Adw.Toast({
                title: _("%game% trashed").replace("%game%", game.name)
            }));

            this.database.gamesCollection.remove(game);

            if (this._navigationView.get_visible_page()?.name === "NavigationPageGameDetails") this._navigationView.pop();
        });
    }
}