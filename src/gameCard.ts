import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import { Game } from "./database/models";

const PARAM_FLAGS = GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY | GObject.ParamFlags.STATIC_NICK | GObject.ParamFlags.STATIC_BLURB;

export default class GameCard extends Gtk.Box {
    public _buttonPlay!: Gtk.Button;
    private _coverNone!: Adw.Clamp;
    private _coverPicture!: Gtk.Picture;
    public _eventControllerMotion!: Gtk.EventControllerMotion;
    public _gestureClickMenu!: Gtk.GestureClick;
    public _mainButton!: Gtk.Button;
    private _menuButton!: Gtk.MenuButton;
    private _menuRevealer!: Gtk.Revealer;
    private _playRevealer!: Gtk.Revealer;
    private _title!: Gtk.Label;

    private _game!: Game | null;
    private database = Database.getInstance();

    static {
        GObject.registerClass({
            GTypeName: "GameCard",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/game-card.ui",
            InternalChildren: [
                "buttonPlay",
                "coverNone",
                "coverPicture",
                "eventControllerMotion",
                "gestureClickMenu",
                "mainButton",
                "menuButton",
                "menuRevealer",
                "playRevealer",
                "title"
            ],
            Properties: {
                Game: GObject.ParamSpec.object("game", "Gane", "The game.", PARAM_FLAGS, GObject.TYPE_OBJECT)
            }
        }, this);
    }

    public cancellable;

    constructor(game: Game | null) {
        super();

        this.game = game;

        this.cancellable = new Gio.Cancellable();

        // Load data on UI
        this.setupUi();

        // Update UI details and send event
        this.database.gamesCollection.connect("item-updated", (_, game) => {
            if (this.game === null) return;

            if (game.id === this.game.id) {
                this.cancellable = new Gio.Cancellable();
                this.game = game;
                this.setupUi();
            }
        });

        // Display revealer on hover
        this._eventControllerMotion.connect("enter", () => this.toggleRevealers(true));
        this._eventControllerMotion.connect("leave", () => this.toggleRevealers(false));
    }

    public get game() { return this._game; }
    set game(game: Game | null) {
        if (this._game === game) return;

        this._game = game;

        this.setupUi();

        this.notify("game");
    }

    private toggleRevealers(value: boolean) {
        // To avoid having menu to close when user hover the menu
        if (!value && this._menuButton.get_popover()?.visible) return;

        this._playRevealer.set_reveal_child(value);
        this._menuRevealer.set_reveal_child(value);
    }

    public toggleHide() {
        if (this.game === null) return;

        this.game.hidden = !this.game.hidden;
        this.database.gamesCollection.update(this.game);
    }

    public setupUi() {
        this._title.set_label(this.game?.name ?? "");

        if (this.game?.coverImage) {
            this.database.getFileAsyncAsImage(this.game.coverImage, this.cancellable)
                .then(texture => this._coverPicture.set_paintable(texture))
                .catch(error => {
                    throw error;
                });


            this._coverPicture.set_visible(true);
            this._coverNone.set_visible(false);
        }
        else {
            this._coverPicture.set_visible(false);
            this._coverNone.set_visible(true);
        }
    }
}