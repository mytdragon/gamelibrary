import Adw from "gi://Adw";
import GdkPixbuf from "gi://GdkPixbuf";
import Gio from "gi://Gio";
import GLib from "gi://GLib";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import Game from "./database/models/game.js";
import Model from "./database/models/model.js";

import { formatDate, secondsToHms } from "./utils/date.js";
import { errorDialog, entryDialog } from "./utils/dialogs.js";

import "./widgets/dropDown.js";
import DropDown from "./widgets/dropDown.js";
import LinkEntry from "./widgets/linkEntry.js";

interface LinkObject extends GObject.Object {
    title: string;
    url: string;
};

type File = {
    file: Gio.File | null;
    is_dirty: boolean;
};

enum Action {
    ADD,
    EDIT
};

const ISO8601_DATE_REGEX = /^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/;

const isValidInt = (number: number, positive = false) => {
    if (isNaN(number)) return false;
    if (positive && number < 0) return false;
    if (!Number.isSafeInteger(number)) return false;
    return true;
};

export default class ManageGameWindow extends Adw.Window {
    private _dropDownAgeRatings!: DropDown;
    private _dropDownCompletionStatus!: DropDown;
    private _dropDownDevelopers!: DropDown;
    private _dropDownGameModes!: DropDown;
    private _dropDownGenres!: DropDown;
    private _dropDownKeywords!: DropDown;
    private _dropDownPlatforms!: DropDown;
    private _dropDownPlayerPerspectives!: DropDown;
    private _dropDownPublishers!: DropDown;
    private _dropDownRegions!: DropDown;
    private _dropDownSeries!: DropDown;
    private _dropDownSource!: DropDown;
    private _dropDownThemes!: DropDown;
    private _entryName!: Gtk.Entry;
    private _entryPlayCount!: Gtk.Entry;
    private _entryPlayTime!: Gtk.Entry;
    private _entryLastPlayed!: Gtk.Entry;
    private _entryReleaseDate!: Gtk.Entry;
    private _entryVersion!: Gtk.Entry;
    private _labelBackgroundSize!: Gtk.Label;
    private _labelCoverSize!: Gtk.Label;
    private _labelCreated!: Gtk.Label;
    private _labelModified!: Gtk.Label;
    private _labelPlayTime!: Gtk.Label;
    private _listBoxLinks!: Gtk.ListBox;
    private _pictureBackground!: Gtk.Picture;
    private _pictureCover!: Gtk.Picture;
    private _spinButtonCommunityRating!: Gtk.SpinButton;
    private _spinButtonCriticRating!: Gtk.SpinButton;
    private _spinButtonUserRating!: Gtk.SpinButton;
    private _switchHidden!: Gtk.Switch;
    private _textViewDescription!: Gtk.TextView;
    private _textViewNotes!: Gtk.TextView;
    private _toastOverlay!: Adw.ToastOverlay;

    private game: Game | undefined;
    private fileBackground: File;
    private fileCover: File;
    private linkStore: Gio.ListStore;

    private database = Database.getInstance();

    static {
        GObject.registerClass({
            GTypeName: "ManageGameWindow",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/manage-game-window.ui",
            InternalChildren: [
                "dropDownAgeRatings",
                "dropDownCompletionStatus",
                "dropDownDevelopers",
                "dropDownGameModes",
                "dropDownGenres",
                "dropDownKeywords",
                "dropDownPlatforms",
                "dropDownPlayerPerspectives",
                "dropDownPublishers",
                "dropDownRegions",
                "dropDownSeries",
                "dropDownSource",
                "dropDownThemes",
                "entryName",
                "entryPlayCount",
                "entryPlayTime",
                "entryLastPlayed",
                "entryReleaseDate",
                "entryVersion",
                "labelBackgroundSize",
                "labelCoverSize",
                "labelCreated",
                "labelModified",
                "labelPlayTime",
                "listBoxLinks",
                "pictureBackground",
                "pictureCover",
                "spinButtonCommunityRating",
                "spinButtonCriticRating",
                "spinButtonUserRating",
                "switchHidden",
                "textViewDescription",
                "textViewNotes",
                "toastOverlay"
            ],
            Signals: {
                "game-added": {
                    param_types: [GObject.TYPE_OBJECT]
                }
            }
        }, this);
    }

    constructor(parent: Gtk.Window, game?: Game) {
        super();

        this.set_transient_for(parent);

        this.game = game;
        this.fileBackground = {file: null, is_dirty: false};
        this.fileCover = {file: null, is_dirty: false};

        this._dropDownAgeRatings.collection = this.database.ageRatingsCollection;
        this._dropDownCompletionStatus.collection = this.database.completionStatusesCollection;
        this._dropDownDevelopers.collection = this.database.developersCollection;
        this._dropDownGameModes.collection = this.database.gameModesCollection;
        this._dropDownGenres.collection = this.database.genresCollection;
        this._dropDownKeywords.collection = this.database.keywordsCollection;
        this._dropDownPlatforms.collection = this.database.platformsCollection;
        this._dropDownPlayerPerspectives.collection = this.database.playerPerspectivesCollection;
        this._dropDownPublishers.collection = this.database.publishersCollection;
        this._dropDownRegions.collection = this.database.regionsCollection;
        this._dropDownSeries.collection = this.database.seriesCollection;
        this._dropDownSource.collection = this.database.sourcesCollection;
        this._dropDownThemes.collection = this.database.themesCollection;

        // Update play time label on play time entry change
        this._entryPlayTime.connect("changed", () => {
            const value = parseInt(this._entryPlayTime.get_text());
            if (!isValidInt(value, true) || value === 0) this._labelPlayTime.set_label(_("Not played"));
            else this._labelPlayTime.set_label(secondsToHms(parseInt(this._entryPlayTime.get_text())));
        });

        // Links
        this.linkStore = Gio.ListStore.new(GObject.TYPE_OBJECT);
        this._listBoxLinks.bind_model(this.linkStore, (gobject) => {
            const linkObject = gobject as LinkObject;
            const linkEntry = new LinkEntry({title: linkObject.title, url: linkObject.url});
            linkEntry.removeButton.connect("clicked", () => {
                const findResult = this.linkStore.find(linkObject);
                if (findResult[0]) this.linkStore.remove(findResult[1]);
            });
            linkEntry.moveDownButton.connect("clicked", () => {
                const findResult = this.linkStore.find(linkObject);
                if (findResult[0]) {
                    const index = findResult[1];
                    if (index === this.linkStore.get_n_items() - 1) return;
                    const linkObject = this.linkStore.get_item(index) as LinkObject;
                    this.linkStore.remove(index);
                    this.linkStore.insert(index + 1, linkObject);
                }                
            });
            linkEntry.moveUpButton.connect("clicked", () => {
                const findResult = this.linkStore.find(linkObject);
                if (findResult[0]) {
                    const index = findResult[1];
                    if (index === 0) return;
                    const linkObject = this.linkStore.get_item(index) as LinkObject;
                    this.linkStore.remove(index);
                    this.linkStore.insert(index - 1, linkObject);
                }                
            });
            linkEntry.connect("notify::title", (linkEntry) => linkObject.title = linkEntry.title);
            linkEntry.connect("notify::url", (linkEntry) => linkObject.url = linkEntry.url);

            return linkEntry;
        });
        for (const link of this.game?.links || []) this.linkStore.append(Object.assign(new GObject.Object(), link));

        // Popuate fields
        if (this.game) {
            this.set_title(_("Edit game details"));
            
            if (this.game.ageRatingIds.length > 0) {
                const ageRatings = this.game.ageRatingIds.map(id => this.database.ageRatingsCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownAgeRatings.selectItems(ageRatings);
            }
            if (this.game.completionStatusId) {
                const completionStatus = this.database.completionStatusesCollection.getById(this.game.completionStatusId);
                if (completionStatus) this._dropDownCompletionStatus.selectItem(completionStatus);
            }
            if (this.game.developerIds.length > 0) {
                const developers = this.game.developerIds.map(id => this.database.developersCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownDevelopers.selectItems(developers);
            }
            if (this.game.gameModeIds.length > 0) {
                const gameModes = this.game.gameModeIds.map(id => this.database.gameModesCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownGameModes.selectItems(gameModes);
            }
            if (this.game.genreIds.length > 0) {
                const genres = this.game.genreIds.map(id => this.database.genresCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownGenres.selectItems(genres);
            }
            if (this.game.keywordIds.length > 0) {
                const keywords = this.game.keywordIds.map(id => this.database.keywordsCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownKeywords.selectItems(keywords);
            }
            if (this.game.platformIds.length > 0) {
                const platforms = this.game.platformIds.map(id => this.database.platformsCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownPlatforms.selectItems(platforms);
            }
            if (this.game.playerPerspectiveIds.length > 0) {
                const playerPerspectives = this.game.playerPerspectiveIds.map(id => this.database.playerPerspectivesCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownPlayerPerspectives.selectItems(playerPerspectives);
            }
            if (this.game.publisherIds.length > 0) {
                const publishers = this.game.publisherIds.map(id => this.database.publishersCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownPublishers.selectItems(publishers);
            }
            if (this.game.regionIds.length > 0) {
                const regions = this.game.regionIds.map(id => this.database.regionsCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownRegions.selectItems(regions);
            }
            if (this.game.serieIds.length > 0) {
                const series = this.game.serieIds.map(id => this.database.seriesCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownSeries.selectItems(series);
            }
            if (this.game.sourceId) {
                const source = this.database.sourcesCollection.getById(this.game.sourceId);
                if (source) this._dropDownSource.selectItem(source);
            }
            if (this.game.themeIds.length > 0) {
                const themes = this.game.themeIds.map(id => this.database.themesCollection.getById(id)).filter(item => item !== null) as Model[];
                this._dropDownThemes.selectItems(themes);
            }

            this._entryLastPlayed.set_text(this.game.lastPlayed ? formatDate(this.game.lastPlayed, "%Y-%m-%d") : "");
            this._entryName.set_text(this.game.name);
            this._entryPlayCount.set_text(this.game.playCount.toString());
            this._entryPlayTime.set_text(this.game.playTime.toString());
            this._entryReleaseDate.set_text(this.game.releaseDate ? formatDate(this.game.releaseDate, "%Y-%m-%d") : "");
            this._entryVersion.set_text(this.game.version ?? "");
            this._labelCreated.set_label(this.game.created ? formatDate(this.game.created, "%Y-%m-%d %H:%M:%S") : "");
            this._labelModified.set_label(this.game.modified ? formatDate(this.game.modified, "%Y-%m-%d %H:%M:%S") : "");
            this._spinButtonCommunityRating.adjustment.set_value(this.game.communityRating ?? 0);
            this._spinButtonCriticRating.adjustment.set_value(this.game.criticRating ?? 0);
            this._spinButtonUserRating.adjustment.set_value(this.game.userRating ?? 0);
            this._switchHidden.set_active(this.game.hidden);
            this._textViewDescription.get_buffer().set_text(this.game.description ?? "", this.game.description?.length ?? 0);
            this._textViewNotes.get_buffer().set_text(this.game.notes ?? "", this.game.notes?.length ?? 0);

            if (this.game.backgroundImage) this._pictureBackground.set_file(this.database.getFile(this.game.backgroundImage));
            if (this.game.coverImage) this._pictureCover.set_file(this.database.getFile(this.game.coverImage));
        }
        else this.set_title(_("Add game"));
    }

    private onAddLinkButtonClicked() {
        this.linkStore.append(Object.assign(new GObject.Object(), {title: "", url: ""}));
    }

    private onBackgroundAddButtonClicked() {
        this.openPictureFile().then(file => {
            if (file) {
                this.fileBackground = {file: file, is_dirty: true};
                this._pictureBackground.set_file(file);

                const path = file.get_path();
                if (path) {
                    const pixbuf = GdkPixbuf.Pixbuf.new_from_file(path);
                    const sizeString = _("%width%x%height%")
                        .replace("%width%", pixbuf.get_width().toString())
                        .replace("%height%", pixbuf.get_height().toString())
                    this._labelBackgroundSize.set_label(sizeString);
                    this._labelBackgroundSize.set_visible(true);
                }
            }
        }).catch(console.error);
    }

    private onBackgroundTrashButtonClicked() {
        this.fileBackground = {file: null, is_dirty: true};
        this._pictureBackground.set_file(null);
        this._labelBackgroundSize.set_visible(false);
    }

    private async onCoverAddButtonClicked() {
        this.openPictureFile().then(file => {
            if (file) {
                this.fileCover = {file: file, is_dirty: true };
                this._pictureCover.set_file(file);

                const path = file.get_path();
                if (path) {
                    const pixbuf = GdkPixbuf.Pixbuf.new_from_file(path);
                    const sizeString = _("%width%x%height%")
                        .replace("%width%", pixbuf.get_width().toString())
                        .replace("%height%", pixbuf.get_height().toString())
                    this._labelCoverSize.set_label(sizeString);
                    this._labelCoverSize.set_visible(true);
                }
            }
        }).catch(console.error);
    }
    
    private onCoverTrashButtonClicked() {
        this.fileCover = {file: null, is_dirty: true};
        this._pictureCover.set_file(null);
        this._labelCoverSize.set_visible(false);
    }

    private onSaveButtonClicked() {
        // Validate data
        const nameValue = this._entryName.get_text();
        if (nameValue === "") {
            errorDialog(this, _("Invalid game data"), _("Game title cannot be empty."));
            return;
        }

        const lastPlayedValue = this._entryLastPlayed.get_text();
        const releaseDateValue = this._entryReleaseDate.get_text();
        const datesValidation = [
            {value: lastPlayedValue, message: _("Last played is an invalid date.") + "\n" + _("The date should be entered in the ISO 8601 format (YYYY-MM-DD).")},
            {value: releaseDateValue, message: _("Release date is an invalid date.") + "\n" + _("The date should be entered in the ISO 8601 format (YYYY-MM-DD).")}
        ];
        for (const element of datesValidation) {
            if (element.value !== "" && !ISO8601_DATE_REGEX.test(element.value)) {
                errorDialog(this, _("Invalid game data"), element.message);
                return;
            }
        }

        const playCountValue = this._entryPlayCount.get_text();
        const playTimeValue = this._entryPlayTime.get_text();
        const positiveIntValidation = [
            {value: playCountValue, message: _("Play count is not a valid positive integer.")},
            {value: playTimeValue, message: _("Play time is not a valid positive integer.")}
        ];
        for (const element of positiveIntValidation) {
            if (element.value && !isValidInt(parseInt(element.value), true)) {
                errorDialog(this, _("Invalid game data"), element.message);
                return;
            }
        }

        // Format data
        const dateNow = new Date();
        const bufferDescription = this._textViewDescription.get_buffer();
        const bufferNotes = this._textViewNotes.get_buffer();
        let links = [];
        for (let i = 0; i < this.linkStore.get_n_items(); i++) {
            const linkObject = this.linkStore.get_item(i) as LinkObject;
            if (linkObject.title === "" || linkObject.url === "") continue;
            links.push({ title: linkObject.title, url: linkObject.url });
        }
        const data = {
            ageRatingIds: (this._dropDownAgeRatings.getSelection() as Model[]).map(ageRating => ageRating.id),
            backgroundImage: this.game?.backgroundImage ?? null,
            completionStatusId: (this._dropDownCompletionStatus.getSelected() as Model | null)?.id ?? null,
            communityRating: this._spinButtonCommunityRating.adjustment.get_value(),
            coverImage: this.game?.coverImage ?? null,
            criticRating: this._spinButtonCriticRating.adjustment.get_value(),
            description: bufferDescription.get_text(bufferDescription.get_start_iter(), bufferDescription.get_end_iter(), true) || null,
            developerIds: (this._dropDownDevelopers.getSelection() as Model[]).map(developer => developer.id),
            gameModeIds: (this._dropDownGameModes.getSelection() as Model[]).map(gameMode => gameMode.id),
            genreIds: (this._dropDownGenres.getSelection() as Model[]).map(genre => genre.id),
            hidden: this._switchHidden.get_active(),
            keywordIds: (this._dropDownKeywords.getSelection() as Model[]).map(keyword => keyword.id),
            modified: dateNow,
            lastPlayed: lastPlayedValue ? new Date(lastPlayedValue) : null,
            links: links,
            name: nameValue,
            notes: bufferNotes.get_text(bufferNotes.get_start_iter(), bufferNotes.get_end_iter(), true) || null,
            platformIds: (this._dropDownPlatforms.getSelection() as Model[]).map(platform => platform.id),
            playCount: playCountValue ? parseInt(playCountValue) : 0,
            playTime: playTimeValue ? parseInt(playTimeValue) : 0,
            playerPerspectiveIds: (this._dropDownPlayerPerspectives.getSelection() as Model[]).map(playerPerspective => playerPerspective.id),
            publisherIds: (this._dropDownPublishers.getSelection() as Model[]).map(publisher => publisher.id),
            regionIds: (this._dropDownRegions.getSelection() as Model[]).map(region => region.id),
            releaseDate: releaseDateValue ? new Date(releaseDateValue) : null,
            serieIds: (this._dropDownSeries.getSelection() as Model[]).map(serie => serie.id),
            sourceId: (this._dropDownSource.getSelected() as Model | null)?.id ?? null,
            themeIds: (this._dropDownThemes.getSelection() as Model[]).map(theme => theme.id),
            userRating: this._spinButtonUserRating.adjustment.get_value(),
            version: this._entryVersion.get_text() || null
        };
        
        
        // Set Game
        let action;
        if (this.game) {
            action = Action.EDIT;
            Object.assign(this.game, data);
        }
        else {
            action = Action.ADD;
            const game = new Game({
                ...data,
                created: dateNow,
                id: GLib.uuid_string_random()
            });
            this.game = game;
        }

        // Store or Trash background
        if (this.fileBackground.is_dirty === true && this.fileBackground.file !== null) {
            const backgroundImage = this.database.addFile(this.fileBackground.file, this.game.id);
            if (backgroundImage) this.game.backgroundImage = backgroundImage;
            else {
                errorDialog(this, _("Cannot save background image"), _("An error has occured while saving the game's background image."));
                return;
            }
        }
        else if (this.fileBackground.is_dirty === true && this.fileBackground.file === null && this.game.backgroundImage) {
            if (this.database.trashFile(this.game.backgroundImage)) this.game.backgroundImage = null;
            else {
                errorDialog(this, _("Cannot trash background image"), _("An error has occured while trashing the game's background image."));
                return;
            }
        }

        // Store or Trash cover
        if (this.fileCover.is_dirty === true && this.fileCover.file !== null) {
            const coverImage = this.database.addFile(this.fileCover.file, this.game.id);
            if (coverImage) this.game.coverImage = coverImage;
            else {
                errorDialog(this, _("Cannot save cover image"), _("An error has occured while saving the game's cover image."));
                return;
            }
        }
        else if (this.fileCover.is_dirty === true && this.fileCover.file === null && this.game.coverImage) {
            if (this.database.trashFile(this.game.coverImage)) this.game.coverImage = null;
            else {
                errorDialog(this, _("Cannot trash cover image"), _("An error has occured while trashing the game's cover image."));
                return;
            }
        }

        if (action === Action.ADD) this.database.gamesCollection.add(this.game);
        else if (action === Action.EDIT) this.database.gamesCollection.update(this.game);

        this.close();
    }

    private async openPictureFile() {
        Gio._promisify(Gtk.FileDialog.prototype, "open", "open_finish");

        const fileFilter = new Gtk.FileFilter({
            mimeTypes: ["image/jpeg"]
        });
        const filters = Gio.ListStore.new(Gtk.FileFilter.$gtype);
        filters.append(fileFilter);
        const fileDialog = new Gtk.FileDialog({
            filters: filters
        });

        // Types generation doesn't detect open as async function
        return (fileDialog.open(this, null) as unknown as Promise<Gio.File>)
            .then((file: Gio.File) => file)
            .catch((error: Gtk.DialogError) => {
                if (error.code !== Gtk.DialogError.DISMISSED) {
                    console.error(error.message);
                    this._toastOverlay.add_toast(new Adw.Toast({
                        title: _("Couldn't open file")
                    }));
                }

                return null;
            });
    }

    private onButtonAddAgeRatingClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const ageRating = this.database.ageRatingsCollection.addFromName(text);
                this._dropDownAgeRatings.selectItem(ageRating);
                
                dialog.close();
            },
            heading: _("Add age rating"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddCompletionStatusClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const completionStatus = this.database.completionStatusesCollection.addFromName(text);
                this._dropDownCompletionStatus.selectItem(completionStatus);
                
                dialog.close();
            },
            heading: _("Add completion status"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddDeveloperClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const developer = this.database.developersCollection.addFromName(text);
                this._dropDownDevelopers.selectItem(developer);
                
                dialog.close();
            },
            heading: _("Add developer"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddGameModeClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const gameMode = this.database.gameModesCollection.addFromName(text);
                this._dropDownGameModes.selectItem(gameMode);
                
                dialog.close();
            },
            heading: _("Add game mode"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddGenreClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const genre = this.database.genresCollection.addFromName(text);
                this._dropDownGenres.selectItem(genre);
                
                dialog.close();
            },
            heading: _("Add genre mode"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddKeywordClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const keyword = this.database.keywordsCollection.addFromName(text);
                this._dropDownKeywords.selectItem(keyword);
                
                dialog.close();
            },
            heading: _("Add keyword mode"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddPlatformClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const platform = this.database.platformsCollection.addFromName(text);
                this._dropDownPlatforms.selectItem(platform);
                
                dialog.close();
            },
            heading: _("Add platform"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddPlayerPerspectiveClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const playerPerspective = this.database.playerPerspectivesCollection.addFromName(text);
                this._dropDownPlayerPerspectives.selectItem(playerPerspective);
                
                dialog.close();
            },
            heading: _("Add player perspective"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddPublisherClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const publisher = this.database.publishersCollection.addFromName(text);
                this._dropDownPublishers.selectItem(publisher);
                
                dialog.close();
            },
            heading: _("Add publisher"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddRegionClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const region = this.database.regionsCollection.addFromName(text);
                this._dropDownRegions.selectItem(region);
                
                dialog.close();
            },
            heading: _("Add region"),
            placeholderText: _("Name"),
            window: this
        });
    }
    private onButtonAddSerieClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const serie = this.database.seriesCollection.addFromName(text);
                this._dropDownSeries.selectItem(serie);
                
                dialog.close();
            },
            heading: _("Add serie"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddSourceClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const source = this.database.sourcesCollection.addFromName(text);
                this._dropDownSource.selectItem(source);
                
                dialog.close();
            },
            heading: _("Add source"),
            placeholderText: _("Name"),
            window: this
        });
    }

    private onButtonAddThemeClicked() {
        entryDialog({
            actionName: _("Add"),
            callbackAction: (dialog, text) => {
                const theme = this.database.themesCollection.addFromName(text);
                this._dropDownThemes.selectItem(theme);
                
                dialog.close();
            },
            heading: _("Add theme"),
            placeholderText: _("Name"),
            window: this
        });
    }
};