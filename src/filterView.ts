import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";
import { Game } from "./database/models/index.js";
import Model from "./database/models/model.js";

import "./widgets/dropDown.js";
import DropDown from "./widgets/dropDown.js";

const PARAM_FLAGS = GObject.ParamFlags.READWRITE | GObject.ParamFlags.EXPLICIT_NOTIFY | GObject.ParamFlags.STATIC_NICK | GObject.ParamFlags.STATIC_BLURB;

export default class FilterView extends Gtk.ScrolledWindow {
    private _dropDownAgeRatings!: DropDown;
    private _dropDownCommunityRatings!: DropDown;
    private _dropDownCompletionStatuses!: DropDown;
    private _dropDownCreateds!: DropDown;
    private _dropDownCriticRatings!: DropDown;
    private _dropDownDevelopers!: DropDown;
    private _dropDownGameModes!: DropDown;
    private _dropDownGenres!: DropDown;
    private _dropDownInstallationSizes!: DropDown;
    private _dropDownKeywords!: DropDown;
    private _dropDownLastPlayeds!: DropDown;
    private _dropDownModifieds!: DropDown;
    private _dropDownPlatforms!: DropDown;
    private _dropDownPlayerPerspectives!: DropDown;
    private _dropDownPlayTimes!: DropDown;
    private _dropDownPublishers!: DropDown;
    private _dropDownRegions!: DropDown;
    private _dropDownReleaseYears!: DropDown;
    private _dropDownSeries!: DropDown;
    private _dropDownSources!: DropDown;
    private _dropDownThemes!: DropDown;
    private _dropDownUserRatings!: DropDown;

    private filterUsedItemsOnly!: boolean;
    private matchAll!: boolean;

    private readonly INSTALL_SIZE_RANGES = [_("None"), "Below 100MB", "100MB to 1GB", "1GB to 5GB", "5GB to 10GB", "10GB to 20GB", "20GB to 40GB", "40GB to 100GB", "Over 100GB"];
    private readonly PLAY_TIME_RANGES = [_("Never"), _("Less than an hour"), _("1 to 10 hours"), _("10 to 100 hours"), _("100 to 500 hours"), _("500 to 1000 hours"), _("Over 1000 hours")];
    private readonly RATING_RANGES = [_("None"), "0x", "1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x", "9x", "100"];
    private readonly TIME_FRAME_RANGES = [_("Never"), _("Today"), _("Yesterday"), _("In the past 7 days"), _("In the past 31 days"), _("In the past 365 days"), _("More than 365 days")];

    private database = Database.getInstance();

    private dates = new Map<number, Date>();

    static {
        GObject.registerClass({
            GTypeName: "FilterView",
            Template: "resource:///dev/mytdragon/GameLibrary/ui/filter-view.ui",
            InternalChildren: [
                "dropDownAgeRatings",
                "dropDownCommunityRatings",
                "dropDownCompletionStatuses",
                "dropDownCreateds",
                "dropDownCriticRatings",
                "dropDownDevelopers",
                "dropDownGameModes",
                "dropDownGenres",
                "dropDownInstallationSizes",
                "dropDownKeywords",
                "dropDownLastPlayeds",
                "dropDownModifieds",
                "dropDownPlatforms",
                "dropDownPlayerPerspectives",
                "dropDownPlayTimes",
                "dropDownPublishers",
                "dropDownRegions",
                "dropDownReleaseYears",
                "dropDownSeries",
                "dropDownSources",
                "dropDownThemes",
                "dropDownUserRatings",
            ],
            Properties: {
                FilterUsedItemsOnly: GObject.ParamSpec.boolean("filter-used-items-only", "Filter used items only", "Show in filter dropdown only the items that are in use.", PARAM_FLAGS, false),
                MatchAll: GObject.ParamSpec.boolean("match-all", "Match all", "Whether to match all the select filters.", PARAM_FLAGS, false)
            },
            Signals: {
                "changed": {
                    param_types: []
                }
            }
        }, this);
    }

    constructor() {
        super();

        // Create the dates so it won't be created for each game check
        this.dates.set(0, new Date());
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        this.dates.set(1, yesterday);
        let aWeekAgo = new Date();
        aWeekAgo.setDate(aWeekAgo.getDate() - 7);
        this.dates.set(7, aWeekAgo);
        let aMonthAgo = new Date();
        aMonthAgo.setDate(aMonthAgo.getDate() - 31);
        this.dates.set(31, aMonthAgo);
        let aYearAgo = new Date();
        aYearAgo.setDate(aYearAgo.getDate() - 365);
        this.dates.set(365, aYearAgo);

        // setup settings and bind settings to inputs
        const settings = new Gio.Settings({ schema_id: "dev.mytdragon.GameLibrary"});
        settings.bind("filter-used-items-only", this, "filter-used-items-only", Gio.SettingsBindFlags.GET);

        // Setup widget's change notification and data
        this.populateDropDownItems();
        this._dropDownAgeRatings.connect("selection-changed", () => this.emit("changed"));
        this._dropDownCommunityRatings.connect("selection-changed", () => this.emit("changed"));
        this._dropDownCompletionStatuses.connect("selection-changed", () => this.emit("changed"));
        this._dropDownCreateds.connect("selection-changed", () => this.emit("changed"));
        this._dropDownCriticRatings.connect("selection-changed", () => this.emit("changed"));
        this._dropDownDevelopers.connect("selection-changed", () => this.emit("changed"));
        this._dropDownGameModes.connect("selection-changed", () => this.emit("changed"));
        this._dropDownGenres.connect("selection-changed", () => this.emit("changed"));
        this._dropDownInstallationSizes.connect("selection-changed", () => this.emit("changed"));
        this._dropDownKeywords.connect("selection-changed", () => this.emit("changed"));
        this._dropDownLastPlayeds.connect("selection-changed", () => this.emit("changed"));
        this._dropDownModifieds.connect("selection-changed", () => this.emit("changed"));
        this._dropDownPlatforms.connect("selection-changed", () => this.emit("changed"));
        this._dropDownPlayerPerspectives.connect("selection-changed", () => this.emit("changed"));
        this._dropDownPlayTimes.connect("selection-changed", () => this.emit("changed"));
        this._dropDownPublishers.connect("selection-changed", () => this.emit("changed"));
        this._dropDownRegions.connect("selection-changed", () => this.emit("changed"));
        this._dropDownReleaseYears.connect("selection-changed", () => this.emit("changed"));
        this._dropDownSeries.connect("selection-changed", () => this.emit("changed"));
        this._dropDownSources.connect("selection-changed", () => this.emit("changed"));
        this._dropDownThemes.connect("selection-changed", () => this.emit("changed"));
        this._dropDownUserRatings.connect("selection-changed", () => this.emit("changed"));
        
        this.connect("notify::match-all", () => this.emit("changed"));
        this.connect("notify::used-items-changed", () => this.populateDropDownItems());
        this.connect("notify::filter-used-items-only", () => this.populateDropDownItems());
        this.database.connect("used-items-changed", () => {
            if (this.filterUsedItemsOnly) this.populateDropDownItems();
        });
    }

    private populateDropDownItems() {
        if (this.filterUsedItemsOnly) {
            this._dropDownAgeRatings.collection = null;
            this._dropDownAgeRatings.expression = this.database.ageRatingsCollection.expression;
            this._dropDownAgeRatings.items = this.database.ageRatingsUsed.map(id => this.database.ageRatingsCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownCompletionStatuses.collection = null;
            this._dropDownCompletionStatuses.expression = this.database.completionStatusesCollection.expression;
            this._dropDownCompletionStatuses.items = this.database.completionStatusesUsed.map(id => this.database.completionStatusesCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownDevelopers.collection = null;
            this._dropDownDevelopers.expression = this.database.developersCollection.expression;
            this._dropDownDevelopers.items = this.database.developersUsed.map(id => this.database.developersCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownGameModes.collection = null;
            this._dropDownGameModes.expression = this.database.gameModesCollection.expression;
            this._dropDownGameModes.items = this.database.gameModesUsed.map(id => this.database.gameModesCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownGenres.collection = null;
            this._dropDownGenres.expression = this.database.genresCollection.expression;
            this._dropDownGenres.items = this.database.genresUsed.map(id => this.database.genresCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownKeywords.collection = null;
            this._dropDownKeywords.expression = this.database.keywordsCollection.expression;
            this._dropDownKeywords.items = this.database.keywordsUsed.map(id => this.database.keywordsCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownPlatforms.collection = null;
            this._dropDownPlatforms.expression = this.database.platformsCollection.expression;
            this._dropDownPlatforms.items = this.database.platformsUsed.map(id => this.database.platformsCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownPlayerPerspectives.collection = null;
            this._dropDownPlayerPerspectives.expression = this.database.playerPerspectivesCollection.expression;
            this._dropDownPlayerPerspectives.items = this.database.playerPerspectivesUsed.map(id => this.database.playerPerspectivesCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownPublishers.collection = null;
            this._dropDownPublishers.expression = this.database.publishersCollection.expression;
            this._dropDownPublishers.items = this.database.publishersUsed.map(id => this.database.publishersCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownRegions.collection = null;
            this._dropDownRegions.expression = this.database.regionsCollection.expression;
            this._dropDownRegions.items = this.database.regionsUsed.map(id => this.database.regionsCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownReleaseYears.items = this.database.releaseYearsUsed.map(year => Gtk.StringObject.new(year.toString()));
            
            this._dropDownSeries.collection = null;
            this._dropDownSeries.expression = this.database.seriesCollection.expression;
            this._dropDownSeries.items = this.database.seriesUsed.map(id => this.database.seriesCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownSources.collection = null;
            this._dropDownSources.expression = this.database.sourcesCollection.expression;
            this._dropDownSources.items = this.database.sourcesUsed.map(id => this.database.sourcesCollection.getById(id)).filter(item => item !== null) as Model[];
            
            this._dropDownThemes.collection = null;
            this._dropDownThemes.expression = this.database.themesCollection.expression;
            this._dropDownThemes.items = this.database.themesUsed.map(id => this.database.themesCollection.getById(id)).filter(item => item !== null) as Model[];
        }
        else {
            this._dropDownAgeRatings.collection = this.database.ageRatingsCollection;
            this._dropDownCompletionStatuses.collection = this.database.completionStatusesCollection;
            this._dropDownDevelopers.collection = this.database.developersCollection;
            this._dropDownGameModes.collection = this.database.gameModesCollection;
            this._dropDownGenres.collection = this.database.genresCollection;
            this._dropDownKeywords.collection = this.database.keywordsCollection;
            this._dropDownPlatforms.collection = this.database.platformsCollection;
            this._dropDownPlayerPerspectives.collection = this.database.playerPerspectivesCollection;
            this._dropDownPublishers.collection = this.database.publishersCollection;
            this._dropDownRegions.collection = this.database.regionsCollection;
            this._dropDownReleaseYears.items = Array.from({ length: new Date().getFullYear() - 1952 + 1 }, (_, index) => Gtk.StringObject.new((1952 + index).toString()));
            this._dropDownSeries.collection = this.database.seriesCollection;
            this._dropDownSources.collection = this.database.sourcesCollection;
            this._dropDownThemes.collection = this.database.themesCollection;
        }
        this._dropDownCommunityRatings.items = this.RATING_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownCreateds.items = this.TIME_FRAME_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownCriticRatings.items = this.RATING_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownInstallationSizes.items = this.INSTALL_SIZE_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownLastPlayeds.items = this.TIME_FRAME_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownModifieds.items = this.TIME_FRAME_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownPlayTimes.items = this.PLAY_TIME_RANGES.map(value => Gtk.StringObject.new(value));
        this._dropDownUserRatings.items = this.RATING_RANGES.map(value => Gtk.StringObject.new(value));
    }

    private clearFilters() {
        this._dropDownAgeRatings.clearSelection();
        this._dropDownCommunityRatings.clearSelection();
        this._dropDownCompletionStatuses.clearSelection();
        this._dropDownCreateds.clearSelection();
        this._dropDownCriticRatings.clearSelection();
        this._dropDownDevelopers.clearSelection();
        this._dropDownGameModes.clearSelection();
        this._dropDownGenres.clearSelection();
        this._dropDownInstallationSizes.clearSelection();
        this._dropDownKeywords.clearSelection();
        this._dropDownLastPlayeds.clearSelection();
        this._dropDownModifieds.clearSelection();
        this._dropDownPlatforms.clearSelection();
        this._dropDownPlayerPerspectives.clearSelection();
        this._dropDownPlayTimes.clearSelection();
        this._dropDownPublishers.clearSelection();
        this._dropDownRegions.clearSelection();
        this._dropDownReleaseYears.clearSelection();
        this._dropDownSeries.clearSelection();
        this._dropDownSources.clearSelection();
        this._dropDownThemes.clearSelection();
        this._dropDownUserRatings.clearSelection();
        this.matchAll = false;
    }

    public check(game: Game) {
        const ageRatings = this._dropDownAgeRatings.getSelection();
        if (
            (this.matchAll && ageRatings.length > 0 && !(game.ageRatingIds.length > 0 && ageRatings.every(item => game.ageRatingIds.includes((item as Model).id))))
            ||
            (!this.matchAll && ageRatings.length > 0 && !(game.ageRatingIds.length > 0 && ageRatings.map(item => (item as Model).id).some(id => game.ageRatingIds.includes(id))))
        ) return false;
        
        const communityRatings = this._dropDownCommunityRatings.getSelection() as Gtk.StringObject[];
        if (this.matchAll && communityRatings.length > 1) return false;
        if (!this.checkRating(game, "communityRating", communityRatings)) return false;

        const completionStatuses = this._dropDownCompletionStatuses.getSelection();
        if (this.matchAll && completionStatuses.length > 1) return false;
        if (completionStatuses.length > 0 && !(game.completionStatusId && completionStatuses.map(item => (item as Model).id).includes(game.completionStatusId))) return false;
        
        const createds = this._dropDownCreateds.getSelection() as Gtk.StringObject[];
        if (this.matchAll && createds.length > 1) return false;
        if (!this.checkTimeFrame(game, "created", createds)) return false;
        
        const criticRatings = this._dropDownCriticRatings.getSelection() as Gtk.StringObject[];
        if (this.matchAll && criticRatings.length > 1) return false;
        if (!this.checkRating(game, "criticRating", criticRatings)) return false;

        const developers = this._dropDownDevelopers.getSelection();
        if (
            (this.matchAll && developers.length > 0 && !(game.developerIds.length > 0 && developers.every(item => game.developerIds.includes((item as Model).id))))
            ||
            (!this.matchAll && developers.length > 0 && !(game.developerIds.length > 0 && developers.map(item => (item as Model).id).some(id => game.developerIds.includes(id))))
        ) return false;

        const gameModes = this._dropDownGameModes.getSelection();
        if (
            (this.matchAll && gameModes.length > 0 && !(game.gameModeIds.length > 0 && gameModes.every(item => game.gameModeIds.includes((item as Model).id))))
            ||
            (!this.matchAll && gameModes.length > 0 && !(game.gameModeIds.length > 0 && gameModes.map(item => (item as Model).id).some(id => game.gameModeIds.includes(id))))
        ) return false;

        const genres = this._dropDownGenres.getSelection();
        if (
            (this.matchAll && genres.length > 0 && !(game.genreIds.length > 0 && genres.every(item => game.genreIds.includes((item as Model).id))))
            ||
            (!this.matchAll && genres.length > 0 && !(game.genreIds.length > 0 && genres.map(item => (item as Model).id).some(id => game.genreIds.includes(id))))
        ) return false;

        const keywords = this._dropDownKeywords.getSelection();
        if (
            (this.matchAll && keywords.length > 0 && !(game.keywordIds.length > 0 && keywords.every(item => game.keywordIds.includes((item as Model).id))))
            ||
            (!this.matchAll && keywords.length > 0 && !(game.keywordIds.length > 0 && keywords.map(item => (item as Model).id).some(id => game.keywordIds.includes(id))))
        ) return false;
        
        const lastPlayeds = this._dropDownLastPlayeds.getSelection() as Gtk.StringObject[];
        if (this.matchAll && lastPlayeds.length > 1) return false;
        if (!this.checkTimeFrame(game, "lastPlayed", lastPlayeds)) return false;
        
        const modifieds = this._dropDownModifieds.getSelection() as Gtk.StringObject[];
        if (this.matchAll && modifieds.length > 1) return false;
        if (!this.checkTimeFrame(game, "modified", modifieds)) return false;

        const platforms = this._dropDownPlatforms.getSelection();
        if (
            (this.matchAll && platforms.length > 0 && !(game.platformIds.length > 0 && platforms.every(item => game.platformIds.includes((item as Model).id))))
            ||
            (!this.matchAll && platforms.length > 0 && !(game.platformIds.length > 0 && platforms.map(item => (item as Model).id).some(id => game.platformIds.includes(id))))
        ) return false;

        const playerPerspecitves = this._dropDownPlatforms.getSelection();
        if (
            (this.matchAll && playerPerspecitves.length > 0 && !(game.playerPerspectiveIds.length > 0 && playerPerspecitves.every(item => game.playerPerspectiveIds.includes((item as Model).id))))
            ||
            (!this.matchAll && playerPerspecitves.length > 0 && !(game.playerPerspectiveIds.length > 0 && playerPerspecitves.map(item => (item as Model).id).some(id => game.playerPerspectiveIds.includes(id))))
        ) return false;
        
        const playTimes = this._dropDownPlayTimes.getSelection() as Gtk.StringObject[];
        if (this.matchAll && playTimes.length > 1) return false;
        if (!this.checkPlayTime(game, playTimes)) return false;

        const publishers = this._dropDownPublishers.getSelection();
        if (
            (this.matchAll && publishers.length > 0 && !(game.publisherIds.length > 0 && publishers.every(item => game.publisherIds.includes((item as Model).id))))
            ||
            (!this.matchAll && publishers.length > 0 && !(game.publisherIds.length > 0 && publishers.map(item => (item as Model).id).some(id => game.publisherIds.includes(id))))
        ) return false;

        const regions = this._dropDownRegions.getSelection();
        if (
            (this.matchAll && regions.length > 0 && !(game.regionIds.length > 0 && regions.every(item => game.regionIds.includes((item as Model).id))))
            ||
            (!this.matchAll && regions.length > 0 && !(game.regionIds.length > 0 && regions.map(item => (item as Model).id).some(id => game.regionIds.includes(id))))
        ) return false;

        const releaseYears = this._dropDownReleaseYears.getSelection() as Gtk.StringObject[];
        if (this.matchAll && releaseYears.length > 1) return false;
        if (releaseYears.length > 0 && (!game.releaseDate?.getFullYear() || !releaseYears.map(o => o.get_string()).includes(game.releaseDate.getFullYear().toString()))) return false;

        const series = this._dropDownSeries.getSelection();
        if (
            (this.matchAll && series.length > 0 && !(game.serieIds.length > 0 && series.every(item => game.serieIds.includes((item as Model).id))))
            ||
            (!this.matchAll && series.length > 0 && !(game.serieIds.length > 0 && series.map(item => (item as Model).id).some(id => game.serieIds.includes(id))))
        ) return false;

        const sources = this._dropDownSources.getSelection();
        if (this.matchAll && sources.length > 1) return false;
        if (sources.length > 0 && !(game.sourceId && sources.map(item => (item as Model).id).includes(game.sourceId))) return false;

        const themes = this._dropDownThemes.getSelection();
        if (
            (this.matchAll && themes.length > 0 && !(game.themeIds.length > 0 && themes.every(item => game.themeIds.includes((item as Model).id))))
            ||
            (!this.matchAll && themes.length > 0 && !(game.themeIds.length > 0 && themes.map(item => (item as Model).id).some(id => game.themeIds.includes(id))))
        ) return false;
        
        const userRatings = this._dropDownUserRatings.getSelection() as Gtk.StringObject[];
        if (this.matchAll && userRatings.length > 1) return false;
        if (!this.checkRating(game, "userRating", userRatings)) return false;

        return true;
    }

    private checkRating(game: Game, key: "communityRating" | "criticRating" | "userRating", selection: Gtk.StringObject[]): boolean {
        // In case there is empty selection
        if (selection.length === 0) return true;

        // Allow all games with null rating when "None" is selected
        if (selection.some(item => item.get_string() === _("None")) && game[key] === null) return true;
        
        // If game's rating is null and "None" isn't selected, it will always return false at this point
        if (game[key] === null) return false;

        // ! is required to tell typescript that it shouldn't be null
        for (const item of selection) {
            switch (item.get_string()) {
                case "0x":
                    if (game[key]! < 10) return true;
                    break;
                
                case "1x":
                    if (game[key]! >= 10 && game[key]! < 20) return true;
                    break;
                
                
                case "2x":
                    if (game[key]! >= 20 && game[key]! < 30) return true;
                    break;
                
                
                case "3x":
                    if (game[key]! >= 30 && game[key]! < 40) return true;
                    break;
                
                
                case "4x":
                    if (game[key]! >= 40 && game[key]! < 50) return true;
                    break;
                
                
                case "5x":
                    if (game[key]! >= 50 && game[key]! < 60) return true;
                    break;
                
                
                case "6x":
                    if (game[key]! >= 60 && game[key]! < 70) return true;
                    break;
                
                
                case "7x":
                    if (game[key]! >= 70 && game[key]! < 80) return true;
                    break;
                
                
                case "8x":
                    if (game[key]! >= 80 && game[key]! < 90) return true;
                    break;
                
                
                case "9x":
                    if (game[key]! >= 90 && game[key]! < 100) return true;
                    break;
                
                
                case "100":
                    if (game[key]! === 100) return true;
                    break;
                
            }
        }

        return false;
    }

    private checkTimeFrame(game: Game, key: "created" | "lastPlayed" | "modified", selection: Gtk.StringObject[]): boolean {
        // In case there is empty selection
        if (selection.length === 0) return true;

        // Allow all games with null date when "None" is selected
        if (selection.some(item => item.get_string() === _("Never")) && game[key] === null) return true;
        
        // If date is null and "None" isn't selected, it will always return false at this point
        if (game[key] === null) return false;

        // ! is required to tell typescript that it shouldn't be null
        for (const item of selection) {
            switch (item.get_string()) {
                case _("Today"):
                    if (game[key]!.toDateString() === this.dates.get(0)!.toDateString()) return true;
                    break;
                
                case _("Yesterday"):
                    if (game[key]!.toDateString() === this.dates.get(1)!.toDateString()) return true;
                    break;
                
                case _("In the past 7 days"):
                    if (game[key]!.getTime() >= this.dates.get(7)!.getTime() && game[key]!.getTime() <= this.dates.get(1)!.getTime()) return true;
                    break;
                
                case _("In the past 31 days"):
                    if (game[key]!.getTime() >= this.dates.get(31)!.getTime() && game[key]!.getTime() <= this.dates.get(7)!.getTime()) return true;
                    break;
                
                
                case _("In the past 365 days"):
                    if (game[key]!.getTime() >= this.dates.get(365)!.getTime() && game[key]!.getTime() <= this.dates.get(31)!.getTime()) return true;
                    break;
                
                
                case _("More than 365 days"):
                    if (game[key]!.getTime() <= this.dates.get(365)!.getTime()) return true;
                    break;
            }
        }

        return false;
    }

    private checkPlayTime(game: Game, selection: Gtk.StringObject[]): boolean {
        // In case there is empty selection
        if (selection.length === 0) return true;

        // Allow all games with play time of 0 when "None" is selected
        if (selection.some(item => item.get_string() === _("Never")) && game.playTime === 0) return true;
        
        // If game's platyime is 0 and "None" isn't selected, it will always return false at this point
        if (game.playTime === 0) return false;

        for (const item of selection) {
            switch (item.get_string()) {
                case _("Less than an hour"):
                    if (game.playTime < 3600) return true;
                    break;
                
                case _("1 to 10 hours"):
                    if (game.playTime >= 3600 && game.playTime < 36000) return true;
                    break;
                
                case _("10 to 100 hours"):
                    if (game.playTime >= 36000 && game.playTime < 360000) return true;
                    break;
                
                case _("100 to 500 hours"):
                    if (game.playTime >= 360000 && game.playTime < 360000 * 5) return true;
                    break;
                
                case _("500 to 1000 hours"):
                    if (game.playTime >= 360000 * 5 && game.playTime < 3600000) return true;
                    break;
                
                
                case _("Over 1000 hours"):
                    if (game.playTime >= 3600000) return true;
                    break;
            }
        }

        return false;
    }
}