import Adw from "gi://Adw";
import Gio from "gi://Gio";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import Database from "./database/database.js";

import GameLibraryWindow from "./window.js";
import PreferencesWindow from "./preferences-window.js";

export class Application extends Adw.Application {
    private window?: GameLibraryWindow;

    static {
        GObject.registerClass(this);
    }

    constructor() {
        super({
            application_id: "dev.mytdragon.GameLibrary",
            flags: Gio.ApplicationFlags.DEFAULT_FLAGS,
        });

        const showAboutAction = new Gio.SimpleAction({ name: "about" });
        showAboutAction.connect("activate", () => {
            const aboutWindow = new Adw.AboutWindow({
                transient_for: this.active_window,
                application_name: _("GameLibrary"),
                application_icon: "dev.mytdragon.GameLibrary",
                developer_name: "mytdragon",
                version: "0.1",
                developers: ["mytdragon <mytdragon@protonmail.com>"],
                copyright: "© 2023 mytdragon",
                license: "MIT"
            });

            aboutWindow.present();
        });
        this.add_action(showAboutAction);

        const preferencesAction = new Gio.SimpleAction({ name: "preferences" });
        preferencesAction.connect("activate", () => {
            const preferencesWindow = new PreferencesWindow();
            preferencesWindow.present();
        });
        this.add_action(preferencesAction);

        const quitAction = new Gio.SimpleAction({ name: "quit" });
        quitAction.connect("activate", () => this.quit());
        this.add_action(quitAction);
        
        this.set_accels_for_action("app.quit", ["<Control>q"]);
        this.set_accels_for_action("app.preferences", ["<primary>comma"]);
        this.set_accels_for_action("win.show-help-overlay", ["<primary>question"]);
        this.set_accels_for_action("win.add-game", ["<Control>n"]);
        this.set_accels_for_action("win.trash-game", ["Delete"]);
        this.set_accels_for_action("win.edit-game", ["<Control>e"]);
        this.set_accels_for_action("win.open-menu", ["F10"]);
        this.set_accels_for_action("win.show-hidden", ["<Control>h"]);
        this.set_accels_for_action("win.toggle-filter-view", ["<Control>g"]);
        this.set_accels_for_action("win.toggle-search", ["<Control>f"]);

        Gio._promisify(Gtk.UriLauncher.prototype, "launch", "launch_finish");
    }

    public vfunc_activate(): void {
        Database.getInstance().openDatabase();

        if (!this.window) {
            this.window = new GameLibraryWindow({ application: this });
        }

        this.window.present();
    }

    public vfunc_startup(): void {
        super.vfunc_startup();
    }
}